package com.imaisnaini.tprint.bl.db.dao;

import com.imaisnaini.tprint.bl.db.model.Alamat;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

public class AlamatDao extends BaseDaoCrud<Alamat, Integer> {
    private static AlamatDao alamatDao;

    public static AlamatDao getAlamatDao() {
        if (alamatDao == null){
            alamatDao = new AlamatDao();
        }
        return alamatDao;
    }

    public List<Alamat> readByUser(String id_user) throws SQLException {
        QueryBuilder<Alamat, Integer> qb = getDao().queryBuilder();
        qb.where().eq(Alamat.ID_USER, id_user);
        return getDao().query(qb.prepare());
    }

    public Alamat readByID(Integer id_file) throws SQLException{
        return getDao().queryForId(id_file);
    }
}
