package com.imaisnaini.tprint.bl.db.dao;

import com.imaisnaini.tprint.bl.db.model.Files;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

public class FileDao extends BaseDaoCrud<Files, String> {
    private static FileDao fileDao;

    public static FileDao getFileDao() {
        if (fileDao == null){
            fileDao = new FileDao();
        }
        return fileDao;
    }

    public List<Files> readByUser(String id_user) throws SQLException {
        QueryBuilder<Files, String> qb = getDao().queryBuilder();
        qb.where().eq(Files.ID_USER, id_user);
        return getDao().query(qb.prepare());
    }

    public Files readByID(String id_file) throws SQLException{
        return getDao().queryForId(id_file);
    }
}
