package com.imaisnaini.tprint.bl.db.dao;


import com.imaisnaini.tprint.bl.db.model.Mitra;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

public class MitraDao extends BaseDaoCrud<Mitra, String> {
    private static MitraDao mitraDao;

    public static MitraDao getMitraDao(){
        if (mitraDao == null){
            mitraDao = new MitraDao();
        }
        return mitraDao;
    }

    public List<Mitra> readByMitra(String id_mitra) throws SQLException {
        QueryBuilder<Mitra, String> qb = getDao().queryBuilder();
        qb.where().eq(Mitra.ID_MITRA, id_mitra);
        return getDao().query(qb.prepare());
    }

    public List<Mitra> getMitraById(String id_mitra) throws SQLException {
        QueryBuilder<Mitra, String> qb = getDao().queryBuilder();
        qb.where().eq(Mitra.ID_MITRA, id_mitra);
        return getDao().query(qb.prepare());
    }
    public Mitra getMitraByID(String id) throws SQLException{
        return getDao().queryForId(id);
    }
}
