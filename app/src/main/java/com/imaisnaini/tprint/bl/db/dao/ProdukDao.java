package com.imaisnaini.tprint.bl.db.dao;

import com.imaisnaini.tprint.bl.db.model.Produk;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

public class ProdukDao extends BaseDaoCrud<Produk, Integer> {
    private static ProdukDao produkDao;

    public static ProdukDao getProdukDao() {
        if (produkDao == null){
            produkDao = new ProdukDao();
        }
        return produkDao;
    }

    public List<Produk> readByMitra(String id_mitra) throws SQLException {
        QueryBuilder<Produk, Integer> qb = getDao().queryBuilder();
        qb.where().eq(Produk.ID_MITRA, id_mitra);
        return getDao().query(qb.prepare());
    }

    public Produk readByID(Integer id_produk) throws SQLException{
        return getDao().queryForId(id_produk);
    }
}
