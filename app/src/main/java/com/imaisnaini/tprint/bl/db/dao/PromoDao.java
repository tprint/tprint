package com.imaisnaini.tprint.bl.db.dao;

import com.imaisnaini.tprint.bl.db.model.Promo;

import java.sql.SQLException;

public class PromoDao extends BaseDaoCrud<Promo, String> {
    private static PromoDao promoDao;

    public static PromoDao getPromoDao() {
        if (promoDao == null){
            promoDao = new PromoDao();
        }
        return promoDao;
    }

    public Promo readByID(String id) throws SQLException{
        return getDao().queryForId(id);
    }
}
