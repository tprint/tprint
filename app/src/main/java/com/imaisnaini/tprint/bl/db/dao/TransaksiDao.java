package com.imaisnaini.tprint.bl.db.dao;

import com.imaisnaini.tprint.bl.db.model.Transaksi;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

public class TransaksiDao extends BaseDaoCrud<Transaksi, Integer>{
    private static TransaksiDao transaksiDao;

    public static TransaksiDao getTransaksiDao(){
        if (transaksiDao == null){
            transaksiDao = new TransaksiDao();
        }
        return transaksiDao;
    }

    public Transaksi getTransaksiByID(String id) throws SQLException {
        QueryBuilder<Transaksi, Integer> qb = getDao().queryBuilder();
        qb.where().eq(Transaksi.ID_TRANSAKSI, id);
        return getDao().query(qb.prepare()).get(0);
    }

    public List<Transaksi> getByStatus(String status) throws SQLException{
        QueryBuilder<Transaksi, Integer> qb = getDao().queryBuilder();
        qb.where().eq(Transaksi.STATUS, status);
        return getDao().query(qb.prepare());
    }


}
