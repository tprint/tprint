package com.imaisnaini.tprint.bl.db.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.imaisnaini.tprint.bl.db.model.Alamat;
import com.imaisnaini.tprint.bl.db.model.Files;
import com.imaisnaini.tprint.bl.db.model.Mitra;
import com.imaisnaini.tprint.bl.db.model.Produk;
import com.imaisnaini.tprint.bl.db.model.Promo;
import com.imaisnaini.tprint.bl.db.model.Transaksi;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DbHelper extends OrmLiteSqliteOpenHelper {
    private static final int DBVER = 1;
    public static final String DBNAME = "tprint.db";

    public DbHelper(Context ctx){
        super(ctx, DBNAME, null, DBVER);
    }
    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Mitra.class);
            TableUtils.createTable(connectionSource, Produk.class);
            TableUtils.createTable(connectionSource, Transaksi.class);
            TableUtils.createTable(connectionSource, Files.class);
            TableUtils.createTable(connectionSource, Alamat.class);
            TableUtils.createTable(connectionSource, Promo.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {}

    @Override
    public ConnectionSource getConnectionSource() {
        return super.getConnectionSource();
    }

}