package com.imaisnaini.tprint.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = Alamat.TBL_NAME)
public class Alamat {
    public static final String TBL_NAME = "alamat";
    public static final String ID_ALAMAT = "id_alamat";
    public static final String ID_USER = "id_user";
    public static final String NAMA = "nama";
    public static final String ALAMAT = "alamat";
    public static final String PROVINSI = "provinsi";
    public static final String KOTA = "kota";
    public static final String KECAMATAN = "kecamatan";
    public static final String KODEPOS = "kodepos";



    @DatabaseField(columnName = ID_ALAMAT, id = true) private Integer id_alamat;
    @DatabaseField(columnName = ID_USER) private String id_user;
    @DatabaseField(columnName = NAMA) private String nama;
    @DatabaseField(columnName = ALAMAT) private String alamat;
    @DatabaseField(columnName = PROVINSI) private String provinsi;
    @DatabaseField(columnName = KOTA) private String kota;
    @DatabaseField(columnName = KECAMATAN) private String kecamatan;
    @DatabaseField(columnName = KODEPOS) private String kodepos;



    public Alamat() {
    }
    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public Integer getId_alamat() {
        return id_alamat;
    }

    public void setId_alamat(Integer id_alamat) {
        this.id_alamat = id_alamat;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKodepos() {
        return kodepos;
    }

    public void setKodepos(String kodepos) {
        this.kodepos = kodepos;
    }
}
