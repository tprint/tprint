package com.imaisnaini.tprint.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = Files.TBL_NAME)
public class Files {
    public static final String TBL_NAME = "file";
    public static final String ID_FILE = "id_file";
    public static final String ID_USER = "id_user";
    public static final String NAMA = "nama";
    public static final String HALAMAN = "halaman";
    public static final String SIZE = "size";

    @DatabaseField(columnName = ID_FILE, id = true) private String id_file;
    @DatabaseField(columnName = ID_USER) private String id_user;
    @DatabaseField(columnName = NAMA) private String nama;
    @DatabaseField(columnName = HALAMAN) private Integer halaman;
    @DatabaseField(columnName = SIZE) private Integer size;

    public Files() {
    }

    public String getId_file() {
        return id_file;
    }

    public void setId_file(String id_file) {
        this.id_file = id_file;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Integer getHalaman() {
        return halaman;
    }

    public void setHalaman(Integer halaman) {
        this.halaman = halaman;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
}
