package com.imaisnaini.tprint.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = Mitra.TBL_NAME)
public class Mitra {
    public static final String TBL_NAME = "mitra";
    public static final String ID_MITRA = "id_mitra";
    public static final String NAMA = "mitra";
    public static final String EMAIL = "email";
    public static final String TELP = "telp";
    public static final String LOGO = "logo";
    public static final String ALAMAT = "alamat";
    public static final String PROV = "prov";
    public static final String KOTA = "kota";
    public static final String KECAMATAN = "kecamatan";
    public static final String KODE_POS = "kode_pos";
    public static final String ID_MAPS = "id_maps";

    @DatabaseField(columnName =  ID_MITRA, id = true) private String id_mitra;
    @DatabaseField(columnName = NAMA) private String nama;
    @DatabaseField(columnName = EMAIL) private String email;
    @DatabaseField(columnName = TELP) private String telp;
    @DatabaseField(columnName = LOGO) private String logo;
    @DatabaseField(columnName = ALAMAT) private String alamat;
    @DatabaseField(columnName = PROV) private String prov;
    @DatabaseField(columnName = KOTA) private String kota;
    @DatabaseField(columnName = KECAMATAN) private String kec;
    @DatabaseField(columnName = KODE_POS) private String kode_pos;
    @DatabaseField(columnName = ID_MAPS) private String id_maps;

    public Mitra() {
    }

    public String getId_mitra() {
        return id_mitra;
    }

    public void setId_mitra(String id_mitra) {
        this.id_mitra = id_mitra;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getProv() {
        return prov;
    }

    public void setProv(String prov) {
        this.prov = prov;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getKec() {
        return kec;
    }

    public void setKec(String kec) {
        this.kec = kec;
    }

    public String getKode_pos() {
        return kode_pos;
    }

    public void setKode_pos(String kode_pos) {
        this.kode_pos = kode_pos;
    }

    public String getId_maps() {
        return id_maps;
    }

    public void setId_maps(String id_maps) {
        this.id_maps = id_maps;
    }
}
