package com.imaisnaini.tprint.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.math.BigDecimal;

@DatabaseTable(tableName = Produk.TBL_NAME)
public class Produk {
    public static final String TBL_NAME = "produk";
    public static final String ID_PRODUK = "id_produk";
    public static final String ID_MITRA = "id_mitra";
    public static final String ID_KATEGORI = "id_kategori";
    public static final String BAHAN = "bahan";
    public static final String UKURAN = "ukuran";
    public static final String KATEGORI = "kategori";
    public static final String ICON = "icon";
    public static final String PRICE_B = "price_b";
    public static final String PRICE_C = "price_c";

    @DatabaseField(columnName = ID_PRODUK, id = true) private Integer id_produk;
    @DatabaseField(columnName = ID_KATEGORI) private Integer id_kategori;
    @DatabaseField(columnName = ID_MITRA) private String id_mitra;
    @DatabaseField(columnName = BAHAN) private String bahan;
    @DatabaseField(columnName = UKURAN) private String ukuran;
    @DatabaseField(columnName = PRICE_B) private BigDecimal price_b;
    @DatabaseField(columnName = PRICE_C) private BigDecimal price_c;
    @DatabaseField(columnName = KATEGORI) private String kategori;
    @DatabaseField(columnName = ICON) private String icon;

    public Produk() {
    }

    public Integer getId_produk() {
        return id_produk;
    }

    public void setId_produk(Integer id_produk) {
        this.id_produk = id_produk;
    }

    public Integer getId_kategori() {
        return id_kategori;
    }

    public void setId_kategori(Integer id_kategori) {
        this.id_kategori = id_kategori;
    }

    public String getId_mitra() {
        return id_mitra;
    }

    public void setId_mitra(String id_mitra) {
        this.id_mitra = id_mitra;
    }

    public String getBahan() {
        return bahan;
    }

    public void setBahan(String bahan) {
        this.bahan = bahan;
    }

    public String getUkuran() {
        return ukuran;
    }

    public void setUkuran(String ukuran) {
        this.ukuran = ukuran;
    }

    public BigDecimal getPrice_b() {
        return price_b;
    }

    public void setPrice_b(BigDecimal price_b) {
        this.price_b = price_b;
    }

    public BigDecimal getPrice_c() {
        return price_c;
    }

    public void setPrice_c(BigDecimal price_c) {
        this.price_c = price_c;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
