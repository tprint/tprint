package com.imaisnaini.tprint.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = Promo.TBL_NAME)
public class Promo {
    public static final String TBL_NAME = "promo";
    public static final String ID_PROMO = "id_promo";
    public static final String ID_MITRA = "id_mitra";
    public static final String NAMA = "nama";
    public static final String BANNER = "banner";
    public static final String DETAIL = "detail";
    public static final String DATE_START = "date_start";
    public static final String DATE_END = "date_end";

    @DatabaseField(columnName = ID_PROMO, id = true) private String id_promo;
    @DatabaseField(columnName = ID_MITRA) private String id_mitra;
    @DatabaseField(columnName = NAMA) private String nama;
    @DatabaseField(columnName = BANNER) private String banner;
    @DatabaseField(columnName = DETAIL) private String detail;
    @DatabaseField(columnName = DATE_START) private String date_start;
    @DatabaseField(columnName = DATE_END) private String date_end;

    public String getId_promo() {
        return id_promo;
    }

    public void setId_promo(String id_promo) {
        this.id_promo = id_promo;
    }

    public String getId_mitra() {
        return id_mitra;
    }

    public void setId_mitra(String id_mitra) {
        this.id_mitra = id_mitra;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getDate_start() {
        return date_start;
    }

    public void setDate_start(String date_start) {
        this.date_start = date_start;
    }

    public String getDate_end() {
        return date_end;
    }

    public void setDate_end(String date_end) {
        this.date_end = date_end;
    }
}
