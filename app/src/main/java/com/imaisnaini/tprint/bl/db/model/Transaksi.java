package com.imaisnaini.tprint.bl.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.math.BigDecimal;

@DatabaseTable(tableName = Transaksi.TBL_NAME)
public class Transaksi {
    public static final String TBL_NAME = "transaksi";
    public static final String ID_TRANSAKSI = "id_transaksi";
    public static final String ID_PRODUK = "id_produk";
    public static final String ID_MITRA = "id_mitra";
    public static final String ID_FILE = "id_file";
    public static final String ID_MAPS = "id_maps";
    public static final String STATUS = "status";
    public static final String TGL_PESAN = "tgl_pesan";
    public static final String TGL_SELESAI = "tgl_selesai";
    public static final String TGL_DELIVERY = "tgl_delivery";
    public static final String PAPER_QTY = "paper_qty";
    public static final String COPY_QTY = "copy_qty";
    public static final String WARNA = "waarna";
    public static final String ONGKIR = "ongkir";
    public static final String HARGA_SATUAN = "harga_satuan";
    public static final String HARGA_TOTAL = "harga_total";
    public static final String KETERANGAN = "keterangan";

    @DatabaseField(columnName = ID_TRANSAKSI, id = true) private String id_transaksi;
    @DatabaseField(columnName = ID_MITRA) private String id_mitra;
    @DatabaseField(columnName = ID_PRODUK) private Integer id_produk;
    @DatabaseField(columnName = ID_FILE) private String id_file;
    @DatabaseField(columnName = ID_MAPS) private String id_maps;
    @DatabaseField(columnName = STATUS) private String status;
    @DatabaseField(columnName = PAPER_QTY) private Integer paper_qty;
    @DatabaseField(columnName = COPY_QTY) private Integer copy_qty;
    @DatabaseField(columnName = WARNA) private Integer warna;
    @DatabaseField(columnName = TGL_PESAN) private String tgl_pesan;
    @DatabaseField(columnName = TGL_SELESAI) private String tgl_selesai;
    @DatabaseField(columnName = TGL_DELIVERY) private String tgl_delivery;
    @DatabaseField(columnName = ONGKIR) private BigDecimal ongkir;
    @DatabaseField(columnName = HARGA_SATUAN) private BigDecimal harga_satuan;
    @DatabaseField(columnName = HARGA_TOTAL) private BigDecimal harga_total;
    @DatabaseField(columnName = KETERANGAN) private String keterangan;

    public Transaksi() {
    }

    public String getId_transaksi() {
        return id_transaksi;
    }

    public void setId_transaksi(String id_transaksi) {
        this.id_transaksi = id_transaksi;
    }

    public String getId_mitra() {
        return id_mitra;
    }

    public void setId_mitra(String id_mitra) {
        this.id_mitra = id_mitra;
    }

    public Integer getId_produk() {
        return id_produk;
    }

    public void setId_produk(Integer id_produk) {
        this.id_produk = id_produk;
    }

    public String getId_file() {
        return id_file;
    }

    public void setId_file(String id_file) {
        this.id_file = id_file;
    }

    public String getId_maps() {
        return id_maps;
    }

    public void setId_maps(String id_maps) {
        this.id_maps = id_maps;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getPaper_qty() {
        return paper_qty;
    }

    public void setPaper_qty(Integer paper_qty) {
        this.paper_qty = paper_qty;
    }

    public Integer getCopy_qty() {
        return copy_qty;
    }

    public void setCopy_qty(Integer copy_qty) {
        this.copy_qty = copy_qty;
    }

    public Integer getWarna() {
        return warna;
    }

    public void setWarna(Integer warna) {
        this.warna = warna;
    }

    public String getTgl_pesan() {
        return tgl_pesan;
    }

    public void setTgl_pesan(String tgl_pesan) {
        this.tgl_pesan = tgl_pesan;
    }

    public String getTgl_selesai() {
        return tgl_selesai;
    }

    public void setTgl_selesai(String tgl_selesai) {
        this.tgl_selesai = tgl_selesai;
    }

    public String getTgl_delivery() {
        return tgl_delivery;
    }

    public void setTgl_delivery(String tgl_delivery) {
        this.tgl_delivery = tgl_delivery;
    }

    public BigDecimal getOngkir() {
        return ongkir;
    }

    public void setOngkir(BigDecimal ongkir) {
        this.ongkir = ongkir;
    }

    public BigDecimal getHarga_satuan() {
        return harga_satuan;
    }

    public void setHarga_satuan(BigDecimal harga_satuan) {
        this.harga_satuan = harga_satuan;
    }

    public BigDecimal getHarga_total() {
        return harga_total;
    }

    public void setHarga_total(BigDecimal harga_total) {
        this.harga_total = harga_total;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
