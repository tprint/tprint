package com.imaisnaini.tprint.bl.network.api;

import com.imaisnaini.tprint.bl.network.config.Config;
import com.imaisnaini.tprint.bl.network.model.AlamatGet;
import com.imaisnaini.tprint.bl.network.model.BaseRespons;
import com.imaisnaini.tprint.bl.network.model.FileGet;
import com.imaisnaini.tprint.bl.network.model.MitraGet;
import com.imaisnaini.tprint.bl.network.model.ProdukGet;
import com.imaisnaini.tprint.bl.network.model.PromoGet;
import com.imaisnaini.tprint.bl.network.model.StorageGet;
import com.imaisnaini.tprint.bl.network.model.TransaksiGet;
import com.imaisnaini.tprint.bl.network.model.User;

import java.math.BigDecimal;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface Api {

    @FormUrlEncoded
    @POST(Config.API_LOGIN)
    Call<User> login(
            @Field("username") String username,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST(Config.API_REGISTER)
    Call<User> register(
            @Field("nama") String nama,
            @Field("email") String email,
            @Field("telp") String telp,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST(Config.API_CREATE_STORAGE)
    Call<BaseRespons> createStorage(
        @Field("id_user") String id_user
    );

    @FormUrlEncoded
    @POST(Config.API_STORAGE)
    Call<StorageGet> readStorage(
            @Field("id_user") String id_user
    );

    @GET(Config.API_MITRA)
    Call<MitraGet> getMitra();

    @GET(Config.API_PRODUK)
    Call<ProdukGet> getProduk();

    @GET(Config.API_PROMOSI)
    Call<PromoGet> getPromo();

    @FormUrlEncoded
    @POST(Config.API_FILE)
    Call<FileGet> getFile(
            @Field("id_user") String id_user
    );

    @Multipart
    @POST(Config.API_UPLOAD_FILE)
    Call<BaseRespons> uploadFile(
            @Part("id_user") String id_user,
            @Part("file") RequestBody name,
            @Part MultipartBody.Part file
    );

    @FormUrlEncoded
    @POST(Config.API_DELETE_FILE)
    Call<BaseRespons> deleteFile(
            @Field("id_user") String id_user,
            @Field("file_name") String file_name
    );

    @FormUrlEncoded
    @POST(Config.API_TRANSAKSI)
    Call<TransaksiGet> getTransaksi(
            @Field("id_user") String id_user
    );

    @FormUrlEncoded
    @POST(Config.API_ADD_ALAMAT_USER)
    Call<BaseRespons> addAlamat(
            @Field("id_user") String id_user,
            @Field("nama") String nama,
            @Field("alamat") String alamat,
            @Field("provinsi") String provinsi,
            @Field("kota") String kota,
            @Field("kecamatan") String kecamatan
    );

    @FormUrlEncoded
    @POST(Config.API_UPDATE_ALAMAT_USER)
    Call<BaseRespons> updateAlamat(
            @Field("id_user") String id_user,
            @Field("id_alamat") Integer id_alamat,
            @Field("nama") String nama,
            @Field("alamat") String alamat,
            @Field("provinsi") String provinsi,
            @Field("kota") String kota,
            @Field("kecamatan") String kecamatan
    );

    @FormUrlEncoded
    @POST(Config.API_DELETE_ALAMAT_USER)
    Call<BaseRespons> deleteAlamat(
            @Field("id_user") String id_user,
            @Field("id_alamat") Integer id_alamat
    );

    @FormUrlEncoded
    @POST(Config.API_ALAMAT_USER)
    Call<AlamatGet> getAlamat(
            @Field("id_user") String id_user
    );

    @FormUrlEncoded
    @POST(Config.API_CHECKOUT)
    Call<BaseRespons> checkout(
            @Field("user") String user,
            @Field("mitra") String mitra,
            @Field("produk") Integer produk,
            @Field("file") String file,
            @Field("paper") Integer paper,
            @Field("copy") Integer copy,
            @Field("warna") Integer warna,
            @Field("harga") BigDecimal harga,
            @Field("total") BigDecimal total,
            @Field("ongkir") BigDecimal ongkir,
            @Field("keterangan") String keterangan,
            @Field("maps") String maps
    );
}
