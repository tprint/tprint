package com.imaisnaini.tprint.bl.network.api;

import android.content.Context;
import android.util.Log;

import com.imaisnaini.tprint.bl.db.dao.AlamatDao;
import com.imaisnaini.tprint.bl.db.dao.FileDao;
import com.imaisnaini.tprint.bl.db.dao.MitraDao;
import com.imaisnaini.tprint.bl.db.dao.ProdukDao;
import com.imaisnaini.tprint.bl.db.dao.PromoDao;
import com.imaisnaini.tprint.bl.db.dao.TransaksiDao;
import com.imaisnaini.tprint.bl.db.model.Alamat;
import com.imaisnaini.tprint.bl.db.model.Files;
import com.imaisnaini.tprint.bl.db.model.Mitra;
import com.imaisnaini.tprint.bl.db.model.Produk;
import com.imaisnaini.tprint.bl.db.model.Promo;
import com.imaisnaini.tprint.bl.db.model.Transaksi;
import com.imaisnaini.tprint.bl.network.model.AlamatGet;
import com.imaisnaini.tprint.bl.network.model.FileGet;
import com.imaisnaini.tprint.bl.network.model.MitraGet;
import com.imaisnaini.tprint.bl.network.model.ProdukGet;
import com.imaisnaini.tprint.bl.network.model.PromoGet;
import com.imaisnaini.tprint.bl.network.model.TransaksiGet;
import com.imaisnaini.tprint.ui.dialog.DialogBuilder;

import java.sql.SQLException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SyncWorker {
    private static SyncWorker syncWorker;

    public static SyncWorker getSyncWorker() {
        if (syncWorker == null){
            syncWorker = new SyncWorker();
        }
        return syncWorker;
    }
    public void syncMitra(final Context ctx, Call<MitraGet> mitraGetCall, final boolean isFirstRun){
        mitraGetCall.enqueue(new Callback<MitraGet>() {
            @Override
            public void onResponse(Call<MitraGet> call, Response<MitraGet> response) {
                if (response.isSuccessful()){
                    MitraGet mitraGet = response.body();
                    Log.i("MITRA_GET", response.message());
                    for (MitraGet.Mitra list :mitraGet.getMitra()) {
                        Mitra mitra = new Mitra();
                        mitra.setId_mitra(list.getIdMitra());
                        mitra.setNama(list.getNama());
                        mitra.setEmail(list.getEmail());
                        mitra.setTelp(list.getTelp());
                        mitra.setLogo(list.getLogo());
                        mitra.setAlamat(list.getAlamat());
                        mitra.setProv(list.getProvinsi());
                        mitra.setKota(list.getKota());
                        mitra.setKec(list.getKecamatan());
                        mitra.setKode_pos(list.getKodePos());
                        mitra.setId_maps(list.getIdMaps());

                        try {
                            if (isFirstRun) {
                                MitraDao.getMitraDao().add(mitra);
                            }else {
                                MitraDao.getMitraDao().save(mitra);
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<MitraGet> call, Throwable t) {
                DialogBuilder.showErrorDialog(ctx, t.getMessage());
                Log.i("MITRA_GET", t.getMessage());
            }
        });
    }

    public void syncProduk(final Context ctx, Call<ProdukGet> produkGetCall, final boolean isFirstRun){
        produkGetCall.enqueue(new Callback<ProdukGet>() {
            @Override
            public void onResponse(Call<ProdukGet> call, Response<ProdukGet> response) {
                if (response.isSuccessful()){
                    ProdukGet produkGet = response.body();
                    Log.i("PRODUK_GET", response.message());
                    for (ProdukGet.Produk data : produkGet.getProduk()) {
                        Produk produk = new Produk();
                        produk.setId_produk(data.getIdProduk());
                        produk.setId_kategori(data.getIdKategori());
                        produk.setId_mitra(data.getIdMitra());
                        produk.setKategori(data.getKategori());
                        produk.setBahan(data.getBahan());
                        produk.setUkuran(data.getUkuran());
                        produk.setPrice_b(data.getPriceB());
                        produk.setPrice_c(data.getPriceC());
                        produk.setIcon(data.getIcon());

                        try {
                            if (isFirstRun) {
                                ProdukDao.getProdukDao().add(produk);
                            }else {
                                ProdukDao.getProdukDao().save(produk);
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ProdukGet> call, Throwable t) {
                Log.i("PRODUK_GET", t.getMessage());
            }
        });
    }

    public void syncTransaksi(final Context ctx, Call<TransaksiGet> transaksiGetCall, final boolean isFirstRun){
        transaksiGetCall.enqueue(new Callback<TransaksiGet>() {
            @Override
            public void onResponse(Call<TransaksiGet> call, Response<TransaksiGet> response) {
                if (response.isSuccessful()){
                    TransaksiGet transaksiGet = response.body();
                    Log.i("TRANSAKSI_GET", response.message());
                    for (TransaksiGet.Transaksi data : transaksiGet.getTransaksi()) {
                        Transaksi transaksi = new Transaksi();
                        transaksi.setId_transaksi(data.getIdTransaksi());
                        transaksi.setId_mitra(data.getIdMitra());
                        transaksi.setId_produk(data.getIdProduk());
                        transaksi.setId_file(data.getIdFile());
                        transaksi.setStatus(data.getStatus());
                        transaksi.setPaper_qty(data.getPaperQty());
                        transaksi.setCopy_qty(data.getCopyQty());
                        transaksi.setWarna(data.getWarna());
                        transaksi.setTgl_pesan(data.getTglPesan());
                        transaksi.setTgl_selesai(data.getTglSelesai());
                        transaksi.setTgl_delivery(data.getTglDelivery());
                        transaksi.setOngkir(data.getOngkir());
                        transaksi.setHarga_satuan(data.getHargaSatuan());
                        transaksi.setHarga_total(data.getHargaTotal());
                        transaksi.setKeterangan(data.getKeterangan());
                        try {
                            if (isFirstRun) {
                                TransaksiDao.getTransaksiDao().add(transaksi);
                            }else {
                                TransaksiDao.getTransaksiDao().save(transaksi);
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<TransaksiGet> call, Throwable t) {
                Log.i("TRANSAKSI_GET", t.getMessage());
            }
        });
    }

    public void syncFile(final Context ctx, Call<FileGet> fileGetCall, final boolean isFirstRun){
        fileGetCall.enqueue(new Callback<FileGet>() {
            @Override
            public void onResponse(Call<FileGet> call, Response<FileGet> response) {
                if (response.isSuccessful()){
                    FileGet fileGet = response.body();
                    Log.i("FILES", response.message());
                    for (FileGet.Files data : fileGet.getFile()) {
                        Files file = new Files();
                        file.setId_file(data.getIdFile());
                        file.setId_user(data.getIdUser());
                        file.setNama(data.getNama());
                        file.setHalaman(data.getHalaman());
                        file.setSize(data.getSize());

                        try {
                            if (isFirstRun) {
                                FileDao.getFileDao().add(file);
                            }else {
                                FileDao.getFileDao().save(file);
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<FileGet> call, Throwable t) {
                Log.i("FILE_GET", t.getMessage());
            }
        });
    }

    public void syncAlamat(final Context ctx, Call<AlamatGet> alamtGetCall, final boolean isFirstRun){
        alamtGetCall.enqueue(new Callback<AlamatGet>() {
            @Override
            public void onResponse(Call<AlamatGet> call, Response<AlamatGet> response) {
                if (response.isSuccessful()){
                    AlamatGet alamatGet = response.body();
                    Log.i("Alamat_GET", response.message());
                    for (AlamatGet.Alamat data : alamatGet.getAlamatList()) {
                        Alamat alamat = new Alamat();
                        alamat.setId_alamat(data.getId_alamat());
                        alamat.setNama(data.getNama());
                        alamat.setAlamat(data.getAlamat());
                        alamat.setProvinsi(data.getProvinsi());
                        alamat.setKecamatan(data.getKecamatan());
                        alamat.setKota(data.getKota());
                        alamat.setKodepos(data.getKodepos());

                        try {
                            if (isFirstRun) {
                                AlamatDao.getAlamatDao().add(alamat);
                            }else {
                                AlamatDao.getAlamatDao().save(alamat);
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<AlamatGet> call, Throwable t) {
                Log.i("Alamat_GET", t.getMessage());
            }
        });
    }

    public void syncPromo(final Context ctx, Call<PromoGet> promoGetCall, final boolean isFirstRun){
        promoGetCall.enqueue(new Callback<PromoGet>() {
            @Override
            public void onResponse(Call<PromoGet> call, Response<PromoGet> response) {
                if (response.isSuccessful()){
                    PromoGet promoGet = response.body();
                    Log.i("PROMO_GET", response.message());
                    for (PromoGet.Promo data : promoGet.getPromo()){
                        Promo promo =  new Promo();
                        promo.setId_promo(data.getIdPromo());
                        promo.setId_mitra(data.getIdMitra());
                        promo.setNama(data.getNama());
                        promo.setDetail(data.getDetail());
                        promo.setBanner(data.getBanner());
                        promo.setDate_start(data.getDateStart());
                        promo.setDate_end(data.getDateEnd());
                        try {
                            if (isFirstRun) {
                                PromoDao.getPromoDao().add(promo);
                            }else {
                                PromoDao.getPromoDao().save(promo);
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }               
            public void onFailure(Call<PromoGet> call, Throwable t) {
                Log.i("PROMO_GET", t.getMessage());
            }
        });
    }
}
