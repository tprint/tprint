package com.imaisnaini.tprint.bl.network.config;

public class Config {
    public static final String BASE_URL = "http://tprint.web.id"; // Your Local IP Address or Localhost (http://10.0.2.2/)

    public static final String API_URL = BASE_URL + "/admin/api";
    public static final String API_URL_ICON = BASE_URL + "/admin/upload";

    public static final String API_LOGIN = API_URL + "/user/login.php";
    public static final String API_REGISTER = API_URL + "/user/register.php";
    public static final String API_MITRA = API_URL + "/mitra/read.php";
    public static final String API_ICON_MITRA = API_URL_ICON + "/mitra_profile/";
    public static final String API_ICON_KATEGORI = API_URL_ICON + "/kategori/";
    public static final String API_ICON_PROMOSI = API_URL_ICON + "/promosi/";
    public static final String API_PROMOSI = API_URL + "/promo/read.php";
    public static final String API_PRODUK = API_URL + "/produk/read.php";
    public static final String API_FILE = API_URL + "/file/read.php";
    public static final String API_UPLOAD_FILE = API_URL + "/file/upload.php";
    public static final String API_DELETE_FILE = API_URL + "/file/delete.php";
    public static final String API_TRANSAKSI = API_URL + "/transaksi/read.php";
    public static final String API_CHECKOUT = API_URL + "/transaksi/checkout.php";
    public static final String API_CREATE_STORAGE = API_URL + "/storage/create.php";
    public static final String API_STORAGE = API_URL + "/storage/read.php";
    public static final String API_ALAMAT_USER = API_URL + "/alamat/read.php";
    public static final String API_ADD_ALAMAT_USER = API_URL + "/alamat/add.php";
    public static final String API_DELETE_ALAMAT_USER = API_URL + "/alamat/delete.php";
    public static final String API_UPDATE_ALAMAT_USER = API_URL + "/alamat/update.php";
}
