package com.imaisnaini.tprint.bl.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AlamatGet {
    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("alamat")
    @Expose
    private List<Alamat> alamatList;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public List<Alamat> getAlamatList() {
        return alamatList;
    }

    public void setAlamatList(List<Alamat> alamatList) {
        this.alamatList = alamatList;
    }

    public class Alamat {
        @SerializedName("id_alamat")
        @Expose
        private Integer id_alamat;
        @SerializedName("nama")
        @Expose
        private String nama;
        @SerializedName("alamat")
        @Expose
        private String alamat;
        @SerializedName("provinsi")
        @Expose
        private String provinsi;
        @SerializedName("kota")
        @Expose
        private String kota;
        @SerializedName("kecamatan")
        @Expose
        private String kecamatan;
        @SerializedName("kodepos")
        @Expose
        private String kodepos;

        public Integer getId_alamat() {
            return id_alamat;
        }

        public void setId_alamat(Integer id_alamat) {
            this.id_alamat = id_alamat;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public String getAlamat() {
            return alamat;
        }

        public void setAlamat(String alamat) {
            this.alamat = alamat;
        }

        public String getProvinsi() {
            return provinsi;
        }

        public void setProvinsi(String provinsi) {
            this.provinsi = provinsi;
        }

        public String getKota() {
            return kota;
        }

        public void setKota(String kota) {
            this.kota = kota;
        }

        public String getKecamatan() {
            return kecamatan;
        }

        public void setKecamatan(String kecamatan) {
            this.kecamatan = kecamatan;
        }

        public String getKodepos() {
            return kodepos;
        }

        public void setKodepos(String kodepos) {
            this.kodepos = kodepos;
        }
    }
}
