package com.imaisnaini.tprint.bl.network.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.imaisnaini.tprint.bl.db.model.Files;

public class FileGet {

    @SerializedName("file")
    @Expose
    private List<Files> file = null;

    public List<Files> getFile() {
        return file;
    }

    public void setFile(List<Files> file) {
        this.file = file;
    }

    public class Files {

        @SerializedName("id_file")
        @Expose
        private String idFile;
        @SerializedName("id_user")
        @Expose
        private String idUser;
        @SerializedName("nama")
        @Expose
        private String nama;
        @SerializedName("halaman")
        @Expose
        private Integer halaman;
        @SerializedName("size")
        @Expose
        private Integer size;

        public String getIdFile() {
            return idFile;
        }

        public void setIdFile(String idFile) {
            this.idFile = idFile;
        }

        public String getIdUser() {
            return idUser;
        }

        public void setIdUser(String idUser) {
            this.idUser = idUser;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public Integer getHalaman() {
            return halaman;
        }

        public void setHalaman(Integer halaman) {
            this.halaman = halaman;
        }

        public Integer getSize() {
            return size;
        }

        public void setSize(Integer size) {
            this.size = size;
        }

    }
}
