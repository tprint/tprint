package com.imaisnaini.tprint.bl.network.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MitraGet {

    @SerializedName("mitra")
    @Expose
    private List<Mitra> mitra = null;

    public List<Mitra> getMitra() {
        return mitra;
    }

    public void setMitra(List<Mitra> mitra) {
        this.mitra = mitra;
    }

    public class Mitra {

        @SerializedName("id_mitra")
        @Expose
        private String idMitra;
        @SerializedName("nama")
        @Expose
        private String nama;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("telp")
        @Expose
        private String telp;
        @SerializedName("logo")
        @Expose
        private String logo;
        @SerializedName("alamat")
        @Expose
        private String alamat;
        @SerializedName("provinsi")
        @Expose
        private String provinsi;
        @SerializedName("kota")
        @Expose
        private String kota;
        @SerializedName("kecamatan")
        @Expose
        private String kecamatan;
        @SerializedName("kode_pos")
        @Expose
        private String kodePos;
        @SerializedName("id_maps")
        @Expose
        private String idMaps;

        public String getIdMitra() {
            return idMitra;
        }

        public void setIdMitra(String idMitra) {
            this.idMitra = idMitra;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getTelp() {
            return telp;
        }

        public void setTelp(String telp) {
            this.telp = telp;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getAlamat() {
            return alamat;
        }

        public void setAlamat(String alamat) {
            this.alamat = alamat;
        }

        public String getProvinsi() {
            return provinsi;
        }

        public void setProvinsi(String provinsi) {
            this.provinsi = provinsi;
        }

        public String getKota() {
            return kota;
        }

        public void setKota(String kota) {
            this.kota = kota;
        }

        public String getKecamatan() {
            return kecamatan;
        }

        public void setKecamatan(String kecamatan) {
            this.kecamatan = kecamatan;
        }

        public String getKodePos() {
            return kodePos;
        }

        public void setKodePos(String kodePos) {
            this.kodePos = kodePos;
        }

        public String getIdMaps() {
            return idMaps;
        }

        public void setIdMaps(String idMaps) {
            this.idMaps = idMaps;
        }
    }
}