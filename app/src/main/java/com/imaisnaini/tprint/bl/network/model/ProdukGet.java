package com.imaisnaini.tprint.bl.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.List;

public class ProdukGet {

    @SerializedName("produk")
    @Expose
    private List<Produk> produk = null;

    public List<Produk> getProduk() {
        return produk;
    }

    public void setProduk(List<Produk> produk) {
        this.produk = produk;
    }
    public class Produk {

        @SerializedName("id_produk")
        @Expose
        private Integer idProduk;
        @SerializedName("id_mitra")
        @Expose
        private String idMitra;
        @SerializedName("id_kategori")
        @Expose
        private Integer idKategori;
        @SerializedName("ukuran")
        @Expose
        private String ukuran;
        @SerializedName("bahan")
        @Expose
        private String bahan;
        @SerializedName("price_c")
        @Expose
        private BigDecimal priceC;
        @SerializedName("price_b")
        @Expose
        private BigDecimal priceB;
        @SerializedName("kategori")
        @Expose
        private String kategori;
        @SerializedName("icon")
        @Expose
        private String icon;

        public Integer getIdProduk() {
            return idProduk;
        }

        public void setIdProduk(Integer idProduk) {
            this.idProduk = idProduk;
        }

        public String getIdMitra() {
            return idMitra;
        }

        public void setIdMitra(String idMitra) {
            this.idMitra = idMitra;
        }

        public Integer getIdKategori() {
            return idKategori;
        }

        public void setIdKategori(Integer idKategori) {
            this.idKategori = idKategori;
        }

        public String getUkuran() {
            return ukuran;
        }

        public void setUkuran(String ukuran) {
            this.ukuran = ukuran;
        }

        public String getBahan() {
            return bahan;
        }

        public void setBahan(String bahan) {
            this.bahan = bahan;
        }

        public BigDecimal getPriceC() {
            return priceC;
        }

        public void setPriceC(BigDecimal priceC) {
            this.priceC = priceC;
        }

        public BigDecimal getPriceB() {
            return priceB;
        }

        public void setPriceB(BigDecimal priceB) {
            this.priceB = priceB;
        }

        public String getKategori() {
            return kategori;
        }

        public void setKategori(String kategori) {
            this.kategori = kategori;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

    }
}