package com.imaisnaini.tprint.bl.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PromoGet {
    @SerializedName("promo")
    @Expose
    private List<Promo> promo = null;

    public List<Promo> getPromo() {
        return promo;
    }

    public void setPromo(List<Promo> promo) {
        this.promo = promo;
    }

    public class Promo {

        @SerializedName("id_promo")
        @Expose
        private String idPromo;
        @SerializedName("id_mitra")
        @Expose
        private String idMitra;
        @SerializedName("nama")
        @Expose
        private String nama;
        @SerializedName("detail")
        @Expose
        private String detail;
        @SerializedName("banner")
        @Expose
        private String banner;
        @SerializedName("dateStart")
        @Expose
        private String dateStart;
        @SerializedName("dateEnd")
        @Expose
        private String dateEnd;

        public String getIdPromo() {
            return idPromo;
        }

        public void setIdPromo(String idPromo) {
            this.idPromo = idPromo;
        }

        public String getIdMitra() {
            return idMitra;
        }

        public void setIdMitra(String idMitra) {
            this.idMitra = idMitra;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        public String getBanner() {
            return banner;
        }

        public void setBanner(String banner) {
            this.banner = banner;
        }

        public String getDateStart() {
            return dateStart;
        }

        public void setDateStart(String dateStart) {
            this.dateStart = dateStart;
        }

        public String getDateEnd() {
            return dateEnd;
        }

        public void setDateEnd(String dateEnd) {
            this.dateEnd = dateEnd;
        }

    }
}
