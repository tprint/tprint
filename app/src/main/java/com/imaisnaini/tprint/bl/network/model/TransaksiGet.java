package com.imaisnaini.tprint.bl.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.List;

public class TransaksiGet {
    @SerializedName("transaksi")
    @Expose
    private List<Transaksi> transaksi = null;

    public List<Transaksi> getTransaksi() {
        return transaksi;
    }

    public void setTransaksi(List<Transaksi> transaksi) {
        this.transaksi = transaksi;
    }

    public class Transaksi {

        @SerializedName("id_transaksi")
        @Expose
        private String idTransaksi;
        @SerializedName("id_mitra")
        @Expose
        private String idMitra;
        @SerializedName("id_produk")
        @Expose
        private Integer idProduk;
        @SerializedName("id_file")
        @Expose
        private String idFile;
        @SerializedName("id_maps")
        @Expose
        private String idMaps;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("tgl_pesan")
        @Expose
        private String tglPesan;
        @SerializedName("tgl_delivery")
        @Expose
        private String tglDelivery;
        @SerializedName("tgl_selesai")
        @Expose
        private String tglSelesai;
        @SerializedName("paper_qty")
        @Expose
        private Integer paperQty;
        @SerializedName("copy_qty")
        @Expose
        private Integer copyQty;
        @SerializedName("warna")
        @Expose
        private Integer warna;
        @SerializedName("ongkir")
        @Expose
        private BigDecimal ongkir;
        @SerializedName("harga_satuan")
        @Expose
        private BigDecimal hargaSatuan;
        @SerializedName("harga_total")
        @Expose
        private BigDecimal hargaTotal;
        @SerializedName("keterangan")
        @Expose
        private String keterangan;

        public String getIdTransaksi() {
            return idTransaksi;
        }

        public void setIdTransaksi(String idTransaksi) {
            this.idTransaksi = idTransaksi;
        }

        public String getIdMitra() {
            return idMitra;
        }

        public void setIdMitra(String idMitra) {
            this.idMitra = idMitra;
        }

        public Integer getIdProduk() {
            return idProduk;
        }

        public void setIdProduk(Integer idProduk) {
            this.idProduk = idProduk;
        }

        public String getIdFile() {
            return idFile;
        }

        public void setIdFile(String idFile) {
            this.idFile = idFile;
        }

        public String getIdMaps() {
            return idMaps;
        }

        public void setIdMaps(String idMaps) {
            this.idMaps = idMaps;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getTglPesan() {
            return tglPesan;
        }

        public void setTglPesan(String tglPesan) {
            this.tglPesan = tglPesan;
        }

        public String getTglDelivery() {
            return tglDelivery;
        }

        public void setTglDelivery(String tglDelivery) {
            this.tglDelivery = tglDelivery;
        }

        public String getTglSelesai() {
            return tglSelesai;
        }

        public void setTglSelesai(String tglSelesai) {
            this.tglSelesai = tglSelesai;
        }

        public Integer getPaperQty() {
            return paperQty;
        }

        public void setPaperQty(Integer paperQty) {
            this.paperQty = paperQty;
        }

        public Integer getCopyQty() {
            return copyQty;
        }

        public void setCopyQty(Integer copyQty) {
            this.copyQty = copyQty;
        }

        public Integer getWarna() {
            return warna;
        }

        public void setWarna(Integer warna) {
            this.warna = warna;
        }

        public BigDecimal getOngkir() {
            return ongkir;
        }

        public void setOngkir(BigDecimal ongkir) {
            this.ongkir = ongkir;
        }

        public BigDecimal getHargaSatuan() {
            return hargaSatuan;
        }

        public void setHargaSatuan(BigDecimal hargaSatuan) {
            this.hargaSatuan = hargaSatuan;
        }

        public BigDecimal getHargaTotal() {
            return hargaTotal;
        }

        public void setHargaTotal(BigDecimal hargaTotal) {
            this.hargaTotal = hargaTotal;
        }

        public String getKeterangan() {
            return keterangan;
        }

        public void setKeterangan(String keterangan) {
            this.keterangan = keterangan;
        }

    }
}
