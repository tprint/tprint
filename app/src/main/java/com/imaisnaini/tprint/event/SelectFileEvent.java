package com.imaisnaini.tprint.event;

public class SelectFileEvent {
    public final String idFile;

    public SelectFileEvent(String idFile) {
        this.idFile = idFile;
    }
}
