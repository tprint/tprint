package com.imaisnaini.tprint.ui.activity;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.imaisnaini.tprint.R;
import com.imaisnaini.tprint.ui.fragment.AlamatFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AlamatActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    Integer idAlamat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alamat);
        ButterKnife.bind(this);
        idAlamat = getIntent().getExtras().getInt("id_alamat");
        initView();
    }

    private void initView(){
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        AlamatFragment alamatFragment = new AlamatFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,alamatFragment).commit();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public Integer getIdAlamat() {
        return idAlamat;
    }
}
