package com.imaisnaini.tprint.ui.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.imaisnaini.tprint.R;
import com.imaisnaini.tprint.bl.db.model.Alamat;
import com.imaisnaini.tprint.bl.db.model.Files;
import com.imaisnaini.tprint.bl.db.model.Mitra;
import com.imaisnaini.tprint.bl.db.model.Produk;
import com.imaisnaini.tprint.bl.db.model.Transaksi;
import com.imaisnaini.tprint.bl.network.api.Api;
import com.imaisnaini.tprint.bl.network.config.Config;
import com.imaisnaini.tprint.bl.network.config.RetrofitBuilder;
import com.imaisnaini.tprint.bl.network.model.BaseRespons;
import com.imaisnaini.tprint.bl.network.model.User;
import com.imaisnaini.tprint.ui.presenter.AlamatPresenter;
import com.imaisnaini.tprint.ui.presenter.FilePresenter;
import com.imaisnaini.tprint.ui.presenter.MitraPresenter;
import com.imaisnaini.tprint.ui.presenter.ProdukPresenter;
import com.imaisnaini.tprint.ui.presenter.TransaksiPresenter;
import com.imaisnaini.tprint.ui.util.Currency;
import com.imaisnaini.tprint.ui.util.PrefUtil;
import com.imaisnaini.tprint.ui.view.TransaksiView;
import com.squareup.picasso.Picasso;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckoutActivity extends AppCompatActivity implements TransaksiView {
    @BindView(R.id.toolbar_activityCheckout) Toolbar mToolbar;
    @BindView(R.id.btnCheckout_activityCheckout) Button btnAction;
    @BindView(R.id.tvFileName_layoutDetailTransaksi) TextView tvFileName;
    @BindView(R.id.tvPage_layoutDetailTransaksi) TextView tvPage;
    @BindView(R.id.tvUkuran_layoutDetailTransaksi) TextView tvUkuran;
    @BindView(R.id.tvCopy_layoutDetailTransaksi) TextView tvCopy;
    @BindView(R.id.tvPageSum_layoutDetailTransaksi) TextView tvPageSum;
    @BindView(R.id.tvPageSum2_layoutDetailTransaksi) TextView tvPageSum2;
    @BindView(R.id.tvPrice_layoutDetailTransaksi) TextView tvPrice;
    @BindView(R.id.tvPriceSum_layoutDetailTransaksi) TextView tvPriceSum;
    @BindView(R.id.tvGrantPrice_layoutDetailTransaksi) TextView tvGrantPrince;
    @BindView(R.id.tvShipping_layoutDetailTransaksi) TextView tvShipping;
    @BindView(R.id.tvWarna_layoutDetailTransaksi) TextView tvWarna;
    @BindView(R.id.tvDate_layoutDetailTransaksi) TextView tvDate;
    @BindView(R.id.tvMitra_layoutDetailTransaksi) TextView tvMitra;
    @BindView(R.id.tvMitraAddress_layoutDetailTransaksi) TextView tvMitraAddress;
    @BindView(R.id.tvUser_layoutDetailTransaksi) TextView tvUser;
    @BindView(R.id.tvUserAddress_layoutDetailTransaksi) TextView tvUserAddress;
    @BindView(R.id.ivIcon_layoutDetailTransaksi) ImageView ivIcon;
    @BindView(R.id.ivWarna_layoutDetailTransaksi) ImageView ivWarna;

    private Files mFile;
    private Produk mProduk;
    private Mitra mMitra;
    private Alamat mAlamat;
    private TransaksiPresenter transaksiPresenter;
    private Integer colorMode, copy, idProduk, page, sumPage, idAlamat;
    private BigDecimal price, ongkir, total, grandTotal;
    private String idFile, keterangan;
    private Api mApi;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        ButterKnife.bind(this);
        mApi = RetrofitBuilder.builder(this).create(Api.class);
        user = PrefUtil.getUser(this, PrefUtil.USER_SESSION);
        initData();
        initView();
    }

    private void initData(){
        // get data from intent
        idFile = getIntent().getStringExtra("idFile");
        keterangan = getIntent().getStringExtra("keterangan");
        idAlamat = getIntent().getIntExtra("idAlamat", 0);
        idProduk = getIntent().getIntExtra("idProduk", 0);
        colorMode = getIntent().getIntExtra("warna", 0);
        copy = getIntent().getIntExtra("copy", 1);

        mFile = new FilePresenter().getByID(idFile);
        mProduk = new ProdukPresenter().getProdukByID(idProduk);
        mMitra = new MitraPresenter().getByID(mProduk.getId_mitra());
        mAlamat = new AlamatPresenter().getByID(idAlamat);
        calc();
    }

    private void calc(){
        page = mFile.getHalaman();
        sumPage = page * copy;
        if (colorMode == 0){
            price = mProduk.getPrice_c();
        }else{
            price = mProduk.getPrice_b();
        }
        total = price.multiply(new BigDecimal(sumPage));
        ongkir = new BigDecimal(8000);
        grandTotal = total.add(ongkir);
    }

    private void initView(){
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tvFileName.setText(mFile.getNama());
        tvUkuran.setText(mProduk.getUkuran());
        tvMitra.setText(mMitra.getNama());
        tvMitraAddress.setText(mMitra.getAlamat());
        tvPrice.setText(Currency.formatDefCurrency(price));
        tvPriceSum.setText(Currency.formatDefCurrency(total));
        tvGrantPrince.setText(Currency.formatDefCurrency(grandTotal));
        tvShipping.setText(Currency.formatDefCurrency(ongkir));
        tvCopy.setText(copy.toString());
        tvPageSum.setText(sumPage.toString());
        tvPageSum2.setText(sumPage.toString());
        tvPage.setText(page.toString());
        tvUser.setText(mAlamat.getNama());
        tvUserAddress.setText(mAlamat.getAlamat());
        Picasso.get().load(Config.API_ICON_KATEGORI + mProduk.getIcon()).into(ivIcon);
        if (colorMode == 1){
            tvWarna.setText("Hitam Putih");
            Picasso.get().load(R.drawable.ic_bw_tiles).into(ivWarna);
        }else {
            tvWarna.setText("Berwarna");
            Picasso.get().load(R.drawable.ic_color_tiles).into(ivWarna);
        }
    }

    @OnClick(R.id.btnCheckout_activityCheckout) public void goCheckout(){
        mApi.checkout(user.getIdUser(),
                mProduk.getId_mitra(),
                mProduk.getId_produk(),
                mFile.getId_file(),
                page, copy,
                colorMode,
                price,
                total,
                ongkir,
                keterangan,
                "1234").enqueue(new Callback<BaseRespons>() {
            @Override
            public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                BaseRespons baseRespons = response.body();

                if (baseRespons != null){
                    if (!baseRespons.getError()){
                        Toast.makeText(getApplicationContext(), "Transaksi Berhasil", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(CheckoutActivity.this, MainActivity.class));
                    }
                }
                if (response.code() == 400){
                    Toast.makeText(getApplicationContext(), "Transaksi Failed parameter tidak valid", Toast.LENGTH_SHORT).show();
                }
                if (response.code() == 502){
                    Toast.makeText(getApplicationContext(), "Gagal menyimpan ke database", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BaseRespons> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadTransaksi(List<Transaksi> transaksiList) {

    }
}
