package com.imaisnaini.tprint.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.imaisnaini.tprint.R;
import com.imaisnaini.tprint.bl.db.model.Files;
import com.imaisnaini.tprint.bl.db.model.Mitra;
import com.imaisnaini.tprint.bl.db.model.Produk;
import com.imaisnaini.tprint.bl.db.model.Transaksi;
import com.imaisnaini.tprint.bl.network.config.Config;
import com.imaisnaini.tprint.ui.presenter.FilePresenter;
import com.imaisnaini.tprint.ui.presenter.MitraPresenter;
import com.imaisnaini.tprint.ui.presenter.ProdukPresenter;
import com.imaisnaini.tprint.ui.presenter.TransaksiPresenter;
import com.imaisnaini.tprint.ui.util.Currency;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailTransaksiActivity extends AppCompatActivity {
    private TransaksiPresenter transaksiPresenter;
    private FilePresenter filePresenter;
    private MitraPresenter mitraPresenter;
    private ProdukPresenter produkPresenter;
    private Transaksi mTransaksi;
    private Files mFile;
    private Mitra mMitra;
    private Produk mProduk;
    private String id_transaksi;

    @BindView(R.id.toolbar_activityDetailTransaksi) Toolbar mToolbar;
    @BindView(R.id.btnAction_activityDetailTransaksi) Button btnAction;
    @BindView(R.id.tvFileName_layoutDetailTransaksi) TextView tvFileName;
    @BindView(R.id.tvPage_layoutDetailTransaksi) TextView tvPage;
    @BindView(R.id.tvUkuran_layoutDetailTransaksi) TextView tvUkuran;
    @BindView(R.id.tvCopy_layoutDetailTransaksi) TextView tvCopy;
    @BindView(R.id.tvPageSum_layoutDetailTransaksi) TextView tvPageSum;
    @BindView(R.id.tvPageSum2_layoutDetailTransaksi) TextView tvPageSum2;
    @BindView(R.id.tvPrice_layoutDetailTransaksi) TextView tvPrice;
    @BindView(R.id.tvPriceSum_layoutDetailTransaksi) TextView tvPriceSum;
    @BindView(R.id.tvGrantPrice_layoutDetailTransaksi) TextView tvGrantPrince;
    @BindView(R.id.tvShipping_layoutDetailTransaksi) TextView tvShipping;
    @BindView(R.id.tvWarna_layoutDetailTransaksi) TextView tvWarna;
    @BindView(R.id.tvDate_layoutDetailTransaksi) TextView tvDate;
    @BindView(R.id.tvMitra_layoutDetailTransaksi) TextView tvMitra;
    @BindView(R.id.tvMitraAddress_layoutDetailTransaksi) TextView tvMitraAddress;
    @BindView(R.id.tvUser_layoutDetailTransaksi) TextView tvUser;
    @BindView(R.id.tvUserAddress_layoutDetailTransaksi) TextView tvUserAddress;
    @BindView(R.id.ivIcon_layoutDetailTransaksi) ImageView ivIcon;
    @BindView(R.id.ivWarna_layoutDetailTransaksi) ImageView ivWarna;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_transaksi);
        ButterKnife.bind(this);
        initPresenter();
        initData();
        initViews();
    }

    private void initPresenter(){
        transaksiPresenter = new TransaksiPresenter();
        filePresenter = new FilePresenter();
        mitraPresenter = new MitraPresenter();
        produkPresenter = new ProdukPresenter();
    }

    private void initData(){
        id_transaksi = getIntent().getStringExtra("id");
        mTransaksi = transaksiPresenter.getByID(id_transaksi);
        mFile = filePresenter.getByID(mTransaksi.getId_file());
        mMitra = mitraPresenter.getByID(mTransaksi.getId_mitra());
        mProduk = produkPresenter.getProdukByID(mTransaksi.getId_produk());
    }

    private void initViews(){
        mToolbar.setTitle("Detail Transaksi");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tvFileName.setText(mFile.getNama());
        tvPage.setText(String.valueOf(mTransaksi.getPaper_qty()));
        tvCopy.setText(String.valueOf(mTransaksi.getCopy_qty()));
        tvPageSum.setText(String.valueOf(mTransaksi.getPaper_qty() * mTransaksi.getCopy_qty()));
        tvPageSum2.setText(String.valueOf(mTransaksi.getPaper_qty() * mTransaksi.getCopy_qty()));
        tvPrice.setText(Currency.formatDefCurrency(mTransaksi.getHarga_satuan()));
        tvPriceSum.setText(Currency.formatDefCurrency(mTransaksi.getHarga_total()));
        tvShipping.setText(Currency.formatDefCurrency(mTransaksi.getOngkir()));
        tvGrantPrince.setText(Currency.formatDefCurrency(mTransaksi.getHarga_total().add(mTransaksi.getOngkir())));
        Picasso.get().load(Config.API_ICON_KATEGORI + mProduk.getIcon()).into(ivIcon);
        tvMitra.setText(mMitra.getNama());
        tvMitraAddress.setText(mMitra.getAlamat());

        if (mTransaksi.getWarna() == 0){
           tvWarna.setText("Hitam Putih");
           Picasso.get().load(R.drawable.ic_bw_tiles).into(ivWarna);
        }else {
            tvWarna.setText("Berwarna");
            Picasso.get().load(R.drawable.ic_color_tiles).into(ivWarna);
        }

        // setting view by status
        if (mTransaksi.getStatus().equals("0")){
            btnAction.setText("Konfirmasi Diterima");
            btnAction.setEnabled(false);
            btnAction.setVisibility(View.VISIBLE);
            tvDate.setText(mTransaksi.getTgl_pesan());
        }else if(mTransaksi.getStatus().equals("1")){
            btnAction.setText("Konfirmasi Diterima");
            btnAction.setEnabled(true);
            btnAction.setVisibility(View.VISIBLE);
            tvDate.setText(mTransaksi.getTgl_delivery());
        }else {
            btnAction.setVisibility(View.GONE);
            tvDate.setText(mTransaksi.getTgl_selesai());
        }
    }

    @OnClick(R.id.btnAction_activityDetailTransaksi) public void onClick(){
        Toast.makeText(getApplication(), "Opps lupa belum selesai", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
