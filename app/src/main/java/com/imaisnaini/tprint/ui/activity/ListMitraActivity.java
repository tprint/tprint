package com.imaisnaini.tprint.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.imaisnaini.tprint.ui.adapter.MitraAdapter;
import com.imaisnaini.tprint.bl.db.model.Mitra;

import com.imaisnaini.tprint.R;
import com.imaisnaini.tprint.ui.presenter.MitraPresenter;
import com.imaisnaini.tprint.ui.view.MitraView;
import com.squareup.picasso.Picasso;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListMitraActivity extends AppCompatActivity implements MitraView {
    private MitraPresenter mitraPresenter;
    private MitraAdapter mAdapter;
    private String id_mitra;
    private Mitra mitra;
    private List<Mitra> mList = new ArrayList<>();

    @BindView(R.id.activity_list_mitra_rvContent)
    RecyclerView mRvContent;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    /*
    @BindView(R.id.activity_kategori_mitra_tvTitle)
    TextView mTvKategori;
    */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_mitra);
        ButterKnife.bind(this);
        init();
    }

    public void init(){
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mitraPresenter = new MitraPresenter(this);
        mAdapter = new MitraAdapter(this);
        mRvContent.setLayoutManager(new LinearLayoutManager(this));
        mRvContent.setHasFixedSize(true);
        mitraPresenter.loadData();
        //mTvKategori.setText(getIntent().getStringExtra("kategori"));
    }



    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadMitra(List<Mitra> mList) {
        mAdapter.generate(mList);
        mRvContent.setAdapter(mAdapter);

    }
}
