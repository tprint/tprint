package com.imaisnaini.tprint.ui.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.imaisnaini.tprint.R;
import com.imaisnaini.tprint.ui.fragment.RegisterFragment;
import com.imaisnaini.tprint.ui.fragment.SignInFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {
    @BindView(R.id.frameLayout_activityLogin)
    FrameLayout frameLayout;
    @BindView(R.id.tvSwitch_activityLogin)
    TextView tvSwitch;
    @BindView(R.id.tvCaption_activityLogin)
    TextView tvCaption;

    private boolean mode = true;
    private Fragment fragmentSignIn = new SignInFragment();
    private Fragment fragmentRegister = new RegisterFragment();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        switchFragment();
    }

    @OnClick(R.id.tvSwitch_activityLogin)
    void switchFragment(){
        if (mode){
            getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout_activityLogin, fragmentSignIn).commit();
            mode = false;
            tvCaption.setText(R.string.caption_login);
            tvSwitch.setText(R.string.signup);
        }
        else {
            getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout_activityLogin, fragmentRegister).commit();
            mode = true;
            tvCaption.setText(R.string.caption_signup);
            tvSwitch.setText(R.string.login);
        }
    }

}
