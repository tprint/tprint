package com.imaisnaini.tprint.ui.activity;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.imaisnaini.tprint.R;
import com.imaisnaini.tprint.bl.db.dao.AlamatDao;
import com.imaisnaini.tprint.bl.db.dao.FileDao;
import com.imaisnaini.tprint.bl.db.dao.MitraDao;
import com.imaisnaini.tprint.bl.db.dao.ProdukDao;
import com.imaisnaini.tprint.bl.db.dao.PromoDao;
import com.imaisnaini.tprint.bl.db.dao.TransaksiDao;
import com.imaisnaini.tprint.bl.db.helper.Db;
import com.imaisnaini.tprint.bl.db.model.Alamat;
import com.imaisnaini.tprint.bl.db.model.Files;
import com.imaisnaini.tprint.bl.db.model.Mitra;
import com.imaisnaini.tprint.bl.db.model.Produk;
import com.imaisnaini.tprint.bl.db.model.Promo;
import com.imaisnaini.tprint.bl.db.model.Transaksi;
import com.imaisnaini.tprint.bl.network.api.Api;
import com.imaisnaini.tprint.bl.network.api.SyncWorker;
import com.imaisnaini.tprint.bl.network.config.RetrofitBuilder;
import com.imaisnaini.tprint.bl.network.model.BaseRespons;
import com.imaisnaini.tprint.bl.network.model.StorageGet;
import com.imaisnaini.tprint.bl.network.model.User;
import com.imaisnaini.tprint.ui.dialog.DialogBuilder;
import com.imaisnaini.tprint.ui.fragment.HistoryFragment;
import com.imaisnaini.tprint.ui.fragment.HomeFragment;
import com.imaisnaini.tprint.ui.fragment.ProfileFragment;
import com.imaisnaini.tprint.ui.fragment.StorageFragment;
import com.imaisnaini.tprint.ui.util.PrefUtil;

import java.sql.SQLException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    public static final int STORAGE_PERMISSION = 20;

    @BindView(R.id.bottom_navigation) BottomNavigationView bottomNav;
    @BindView(R.id.tvTitle_toolbarMain)
    TextView tvTitleToolbar;

    private Api mApi;
    private boolean isFirstRun = true;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Db.getInstance().init(this);
        mApi = RetrofitBuilder.builder(this).create(Api.class);
        user = PrefUtil.getUser(this, PrefUtil.USER_SESSION);
//        readStorage(user.getIdUser());
        initialCheck();
        init();
        new DoCloudSync(this).execute();

        //ask permission to read storage
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION);
        }


    }

    // This method to initialaze view
    private void init() {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new HomeFragment()).commit();
        bottomNav.setOnNavigationItemSelectedListener(navListener);
        readStorage(user.getIdUser());
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;

            switch (item.getItemId()){
                case R.id.nav_home:
                    selectedFragment = new HomeFragment();
                    tvTitleToolbar.setText(R.string.home_fragment);
                    break;
                case R.id.nav_storage:
                    selectedFragment = new StorageFragment();
                    tvTitleToolbar.setText(R.string.storage_fragment);
                    break;
                case R.id.nav_history:
                    selectedFragment = new HistoryFragment();
                    tvTitleToolbar.setText(R.string.transaksi_fragment);
                    break;
                case R.id.nav_profile:
                    selectedFragment = new ProfileFragment();
                    tvTitleToolbar.setText(R.string.profile_fragment);
                    break;
            }

            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,selectedFragment).commit();
            return true;
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case STORAGE_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permission Recieved.", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(this, "Permission Denied.", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    private class DoCloudSync extends AsyncTask<Void, Void, Void> {
        private MaterialDialog dialog;
        private final Context ctx;

        private DoCloudSync(Context ctx) {
            this.ctx = ctx;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = DialogBuilder.showLoadingDialog(ctx, "Updating Data", "Please Wait", false);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                SyncWorker.getSyncWorker().syncMitra(ctx, mApi.getMitra(), isFirstRun);
                SyncWorker.getSyncWorker().syncProduk(ctx, mApi.getProduk(), isFirstRun);
                SyncWorker.getSyncWorker().syncFile(ctx,mApi.getFile(user.getIdUser()), isFirstRun);
                SyncWorker.getSyncWorker().syncAlamat(ctx,mApi.getAlamat(user.getIdUser()), isFirstRun);
                SyncWorker.getSyncWorker().syncTransaksi(ctx, mApi.getTransaksi(user.getIdUser()), isFirstRun);
                SyncWorker.getSyncWorker().syncPromo(ctx, mApi.getPromo(), isFirstRun);
                if(isFirstRun) Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dialog.dismiss();
        }
    }

    private void initialCheck(){
        try {
            List<Mitra> mitrasCheck = MitraDao.getMitraDao().read();
            List<Produk> produksCheck = ProdukDao.getProdukDao().read();
            List<Transaksi> transaksisCheck = TransaksiDao.getTransaksiDao().read();
            List<Alamat> alamatCheck = AlamatDao.getAlamatDao().read();
            List<Files> fileCheck = FileDao.getFileDao().read();
            List<Promo> promosCheck = PromoDao.getPromoDao().read();
            isFirstRun = (mitrasCheck.isEmpty() || produksCheck.isEmpty() || transaksisCheck.isEmpty() || alamatCheck.isEmpty()|| promosCheck.isEmpty() || false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Log.i("ISFIRSTRUN", String.valueOf(isFirstRun));
    }

    private void readStorage(String id_user){
        mApi.readStorage(id_user).enqueue(new Callback<StorageGet>() {
            @Override
            public void onResponse(Call<StorageGet> call, Response<StorageGet> response) {
                StorageGet storageRespons = response.body();

                if(storageRespons != null) {
                    //Toast.makeText(getActivity(), user.getId_user(), Toast.LENGTH_SHORT).show();
                    if(!storageRespons.getError()) {
                        //Toast.makeText(getActivity(), baseRespons.getMessage(), Toast.LENGTH_SHORT).show();
                        //Toast.makeText(getActivity(), "Success Read Storage", Toast.LENGTH_SHORT).show();
                        PrefUtil.putStorage(getApplication(), PrefUtil.USER_STORAGE, storageRespons);
                    }
                }
            }

            @Override
            public void onFailure(Call<StorageGet> call, Throwable t) {
                //Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
