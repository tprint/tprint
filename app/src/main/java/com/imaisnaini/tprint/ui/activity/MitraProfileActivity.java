package com.imaisnaini.tprint.ui.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ImageView;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.imaisnaini.tprint.bl.db.model.Files;
import com.imaisnaini.tprint.bl.db.model.Mitra;
import com.imaisnaini.tprint.bl.db.model.Produk;
import com.imaisnaini.tprint.bl.network.config.Config;
import com.imaisnaini.tprint.event.SelectFileEvent;
import com.imaisnaini.tprint.ui.presenter.FilePresenter;
import com.imaisnaini.tprint.ui.presenter.MitraPresenter;
import com.imaisnaini.tprint.ui.presenter.ProdukPresenter;
import com.imaisnaini.tprint.R;


import com.imaisnaini.tprint.ui.util.Currency;
import com.imaisnaini.tprint.ui.view.MitraView;
import com.imaisnaini.tprint.ui.view.ProdukView;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MitraProfileActivity extends AppCompatActivity implements MitraView, ProdukView {
    private Mitra mitra;
    private String id_mitra;
    private String selectedFile;
    private List<Files> filesList = new ArrayList<>();
    private String[] idFiles, namaFiles;
    private MitraPresenter mitraPresenter;
    private ProdukPresenter produkPresenter;
    private FilePresenter filePresenter;
    private ProdukAdapter produkAdapter;

    @BindView(R.id.activity_mitra_tvMitra)
    TextView mTvMitra;
    @BindView(R.id.activity_mitra_tvAlamat)
    TextView mTvAlamat;
    @BindView(R.id.activity_mitra_tvJam)
    TextView mTvJam;
    @BindView(R.id.activity_mitra_rvContent)
    RecyclerView mRvContent;
    @BindView(R.id.activity_mitra_ivIcon)
    ImageView mIvIcon;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mitra_profile);
        ButterKnife.bind(this);
        init();

    }

    private void init(){
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mitraPresenter = new MitraPresenter(this);
        id_mitra = String.valueOf(getIntent().getStringExtra("id_mitra"));
        mitra = mitraPresenter.getByID(id_mitra);
        mTvMitra.setText(mitra.getNama());
        mTvAlamat.setText(mitra.getAlamat());

        produkPresenter= new ProdukPresenter(this);
        filePresenter = new FilePresenter();
        produkAdapter = new ProdukAdapter(this);
        mRvContent.setLayoutManager(new LinearLayoutManager(this));
        mRvContent.setHasFixedSize(true);
//        produkPresenter.loadProdukByMitra(id_mitra);
        //mTvJam.setText(mitra.getJam_buka() + " - "  + mitra.getJam_tutup());
        Picasso.get().load(Config.API_ICON_MITRA + mitra.getLogo()).into(mIvIcon);

        filesList = filePresenter.getFileList();
        idFiles = new String[filesList.size()];
        namaFiles = new String[filesList.size()];
        for (int i = 0; i < filesList.size(); i++){
            idFiles[i] = filesList.get(i).getId_file();
            namaFiles[i] = filesList.get(i).getNama();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        produkPresenter.loadProdukByMitra(id_mitra);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    @Override
    public void showLoading(){

    }

    @Override
    public void hideLoading(){

    }

    @Override
    public void loadProduk(List<Produk> produkList) {
        produkAdapter.generate(produkList);
        mRvContent.setAdapter(produkAdapter);
    }

    @Override
    public void loadMitra(List<Mitra> mitraList) {

    }

    public class ProdukAdapter extends RecyclerView.Adapter<ProdukAdapter.ViewHolder>{

        private List<Produk> produkList = new ArrayList<>();
        Context ctx;
        int flag = 0;

        public ProdukAdapter(Context ctx) {
            this.ctx = ctx;
        }

        public ProdukAdapter(Context ctx, int flag) {
            this.ctx = ctx;
            this.flag = flag;
        }

        public ProdukAdapter(List<Produk> produkList ) {

            this.produkList = produkList;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_produk, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            holder.idProduk = produkList.get(position).getId_produk();
            holder.idMitra = produkList.get(position).getId_mitra();
            holder.tvTitle.setText(produkList.get(position).getKategori() + " : " + produkList.get(position).getUkuran());
            holder.tvDetail.setText("Bahan: " + produkList.get(position).getBahan());
            holder.tvHargaBlack.setText("Rp. " + Currency.formatDefCurrency(produkList.get(position).getPrice_b()));
            holder.tvHargaColor.setText("Rp. " + Currency.formatDefCurrency(produkList.get(position).getPrice_c()));
            Picasso.get().load(Config.API_ICON_KATEGORI + produkList.get(position).getIcon()).into(holder.ivIcon);
        }

        @Override
        public int getItemCount() {

            return produkList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{
            @BindView(R.id.item_produk_tvTitle)
            TextView tvTitle;
            @BindView(R.id.item_produk_tvHargaColor)
            TextView tvHargaColor;
            @BindView(R.id.item_produk_tvHargaBlack)
            TextView tvHargaBlack;
            @BindView(R.id.item_produk_tvDetail)
            TextView tvDetail;
            @BindView(R.id.item_produk_ivIcon)
            ImageView ivIcon;

            private Integer idProduk;
            private String idMitra;

            public ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

            @OnClick(R.id.item_produk_cardView) public void onClick(){
                CFAlertDialog.Builder builder = new CFAlertDialog.Builder(MitraProfileActivity.this)
                        .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                        .setTitle("Pilih File")
                        .setSingleChoiceItems(namaFiles,0, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                selectedFile = idFiles[which];
                            }
                        })
                        .addButton("OK", -1, -1,
                                CFAlertDialog.CFAlertActionStyle.POSITIVE,
                                CFAlertDialog.CFAlertActionAlignment.END,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int i) {
                                        Intent intent = new Intent(ctx, PrintOptionActivity.class);
                                        if (selectedFile == null){
                                            selectedFile = idFiles[0];
                                        }
                                        intent.putExtra("idFile", selectedFile);
                                        intent.putExtra("idProduk", idProduk);
                                        dialog.dismiss();
                                        startActivity(intent);
                                    }
                        });
                builder.show();
            }

        }

        public void generate(List<Produk> list) {
            clear();
            this.produkList = list;
            notifyDataSetChanged();
        }

        public void clear() {
            produkList.clear();
            notifyDataSetChanged();
        }
    }

}
