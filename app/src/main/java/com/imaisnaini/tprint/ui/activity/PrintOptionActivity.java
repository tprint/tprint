package com.imaisnaini.tprint.ui.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.imaisnaini.tprint.R;
import com.imaisnaini.tprint.bl.db.model.Alamat;
import com.imaisnaini.tprint.bl.db.model.Files;
import com.imaisnaini.tprint.bl.db.model.Produk;
import com.imaisnaini.tprint.bl.network.config.Config;
import com.imaisnaini.tprint.ui.presenter.AlamatPresenter;
import com.imaisnaini.tprint.ui.presenter.FilePresenter;
import com.imaisnaini.tprint.ui.presenter.ProdukPresenter;
import com.imaisnaini.tprint.ui.util.Currency;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lib.kingja.switchbutton.SwitchMultiButton;

public class PrintOptionActivity extends AppCompatActivity {
    @BindView(R.id.toolbar_activityPrintOption)
    Toolbar mToolbar;
    @BindView(R.id.etCopyQty_activityPrintOption)
    EditText etCopyQty;
    @BindView(R.id.fabNext_activityPrintOption)
    FloatingActionButton fabNext;
    @BindView(R.id.ivKategori_activityPrintOption)
    ImageView ivKategori;
    @BindView(R.id.etKeteragan_activityPrintOption)
    EditText etKeterangan;
    @BindView(R.id.tvBahan_activityPrintOption)
    TextView tvBahan;
    @BindView(R.id.tvHarga_activityPrintOption)
    TextView tvPrice;
    @BindView(R.id.tvKategori_activityPrintOption)
    TextView tvKategori;
    @BindView(R.id.tvUkuran_activityPrintOption)
    TextView tvSize;
    @BindView(R.id.sbtnWarna_activityPrintOption)
    SwitchMultiButton sbtnWarna;
    @BindView(R.id.tvPage_activityPrintOption)
    TextView tvPage;
    @BindView(R.id.tvFileName_activityPrintOption)
    TextView tvFileName;

    private ProdukPresenter mProdukPresenter;
    private FilePresenter mFilePresenter;
    private AlamatPresenter mAlamatPresenter;
    private Files mFiles;
    private Produk mProduk;
    private String idFile;
    private Integer idProduk, selectedAlamat = 0;
    private List<Alamat> alamatList = new ArrayList<>();
    private Integer[] idAlamat;
    private String[] namaAlamat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print_option);
        ButterKnife.bind(this);
        initPresenter();
        initData();
        initViews();
    }

    private void initPresenter(){
        mFilePresenter = new FilePresenter();
        mProdukPresenter = new ProdukPresenter();
        mAlamatPresenter = new AlamatPresenter();
    }

    private void initData(){
        idFile = getIntent().getStringExtra("idFile");
        idProduk = getIntent().getIntExtra("idProduk", 0);
        mFiles = mFilePresenter.getByID(idFile);
        mProduk = mProdukPresenter.getProdukByID(idProduk);
        alamatList = mAlamatPresenter.getAlamatList();
        idAlamat = new Integer[alamatList.size()];
        namaAlamat = new String[alamatList.size()];
        for (int i = 0; i < alamatList.size(); i++){

            idAlamat[i] = new Integer(alamatList.get(i).getId_alamat());
            namaAlamat[i] = alamatList.get(i).getNama();
        }
    }

    private void initViews(){
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tvFileName.setText(mFiles.getNama());
        tvPage.setText(mFiles.getHalaman() + " pages");
        tvKategori.setText(": " + mProduk.getKategori());
        tvSize.setText(": " + mProduk.getUkuran());
        tvPrice.setText(": Rp. " + Currency.formatDefCurrency(mProduk.getPrice_c()) + ",-");
        Picasso.get().load(Config.API_ICON_KATEGORI + mProduk.getIcon()).into(ivKategori);

        sbtnWarna.setOnSwitchListener(new SwitchMultiButton.OnSwitchListener() {
            @Override
            public void onSwitch(int position, String tabText) {
                if (position == 0){
                    tvPrice.setText(": Rp. " + Currency.formatDefCurrency(mProduk.getPrice_c()) + ",-");
                }else {
                    tvPrice.setText(": Rp. " + Currency.formatDefCurrency(mProduk.getPrice_b()) + ",-");
                }
            }
        });
    }

    @OnClick(R.id.ivCopyMin_activityPrintOption) public void onMinQty(){
        Integer qty = Integer.valueOf(etCopyQty.getText().toString());
        if (!qty.equals(1)){
            etCopyQty.setText(String.valueOf(qty-1));
        }
    }

    @OnClick(R.id.ivCopyAdd_activityPrintOption) public void onAddQty(){
        Integer qty = Integer.valueOf(etCopyQty.getText().toString());
        if (!qty.equals(999)){
            etCopyQty.setText(String.valueOf(qty+1));
        }
    }

    @OnClick(R.id.fabNext_activityPrintOption) public void goCheckout(){
        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(PrintOptionActivity.this)
                .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                .setTitle("Pilih Alamat Pegiriman")
                .setSingleChoiceItems(namaAlamat,0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selectedAlamat = idAlamat[which];
                    }
                })
                .addButton("OK", -1, -1,
                        CFAlertDialog.CFAlertActionStyle.POSITIVE,
                        CFAlertDialog.CFAlertActionAlignment.END,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int i) {
                                Intent intent = new Intent(PrintOptionActivity.this, new CheckoutActivity().getClass());
                                intent.putExtra("idFile", idFile);
                                intent.putExtra("idProduk",  idProduk);
                                intent.putExtra("copy", Integer.valueOf(etCopyQty.getText().toString()));
                                intent.putExtra("warna", sbtnWarna.getSelectedTab());
                                if (selectedAlamat == 0){
                                    selectedAlamat = idAlamat[0];
                                }
                                intent.putExtra("idAlamat", selectedAlamat);
                                intent.putExtra("keterangan", etKeterangan.getText().toString());

                                dialog.dismiss();
                                startActivity(intent);
                            }
                });
        builder.show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
