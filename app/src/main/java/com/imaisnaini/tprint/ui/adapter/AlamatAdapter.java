package com.imaisnaini.tprint.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.imaisnaini.tprint.R;
import com.imaisnaini.tprint.bl.db.dao.AlamatDao;
import com.imaisnaini.tprint.bl.db.model.Alamat;
import com.imaisnaini.tprint.bl.network.api.Api;
import com.imaisnaini.tprint.bl.network.api.SyncWorker;
import com.imaisnaini.tprint.bl.network.config.RetrofitBuilder;
import com.imaisnaini.tprint.bl.network.model.BaseRespons;
import com.imaisnaini.tprint.bl.network.model.User;
import com.imaisnaini.tprint.ui.activity.AlamatActivity;
import com.imaisnaini.tprint.ui.activity.MainActivity;
import com.imaisnaini.tprint.ui.util.PrefUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AlamatAdapter extends RecyclerView.Adapter<AlamatAdapter.ViewHolder> {
    private List<Alamat> mList = new ArrayList<>();
    Context ctx;
    int flag = 0;
    private Api mApi;

    public AlamatAdapter(Context ctx){
        this.ctx = ctx;
        mApi = RetrofitBuilder.builder(ctx).create(Api.class);
    }

    public AlamatAdapter(Context ctx, int flag) {
        this.ctx = ctx;
        this.flag = flag;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_alamat, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvNama.setText(mList.get(position).getNama());
        holder.tvAlamat.setText(mList.get(position).getAlamat()+" "+mList.get(position).getKecamatan()+", "+mList.get(position).getKota()+", "+mList.get(position).getProvinsi());
        holder.id_alamat = mList.get(position).getId_alamat();
        holder.id_user = mList.get(position).getId_user();
        holder.position = position;
    }

    public void removeItem(int position) {
        //int position = mList.indexOf(item);
        Alamat alamat = mList.get(position);
        try {
            AlamatDao.getAlamatDao().delete(alamat);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        mList.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.tvNamaAlamat_itemAlamat)
        TextView tvNama;
        @BindView(R.id.tvAlamat_itemAlamat)
        TextView tvAlamat;
        private int position;
        private String id_user;
        private Integer id_alamat;
        private User user = PrefUtil.getUser(ctx, PrefUtil.USER_SESSION);;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.ivEdit_itemAlamat)
        public void edit(){
            Intent i = new Intent(ctx, AlamatActivity.class);
            i.putExtra("id_alamat", id_alamat);
            ctx.startActivity(i);
        }
        @OnLongClick(R.id.llAlamat_itemAlamat)
        public boolean dialogDelete(){
            MaterialDialog.Builder dialog = new MaterialDialog.Builder(ctx)
                    .title("Delet Alamat")
                    .content("Anda Yakin?")
                    .positiveText("Ya")
                    .negativeText("Tidak")
                    .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                        @Override
                        public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                            return false;
                        }
                    })
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                            super.onPositive(dialog);
                            if (flag == 0) {
                                deleteAlamat(user.getIdUser(),id_alamat,position);
                                SyncWorker.getSyncWorker().syncAlamat(ctx,mApi.getAlamat(id_user), false);
                            }else {
                                dialog.dismiss();
//                                return false;
                            }
                        }

                        @Override
                        public void onNegative(MaterialDialog dialog) {
                            super.onNegative(dialog);
                            dialog.dismiss();
                        }
                    });
            dialog.show();
            return true;
        }
    }

    private void deleteAlamat(String id_user, Integer id_alamat, final Integer position){

        mApi.deleteAlamat(id_user,id_alamat).enqueue(new Callback<BaseRespons>() {
            @Override
            public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                BaseRespons baseRespons = response.body();
                if(baseRespons != null) {
                    //Toast.makeText(getActivity(), user.getId_user(), Toast.LENGTH_SHORT).show();
                    if(!baseRespons.getError()) {
                        notifyDataSetChanged();
                        removeItem(position);
//                        SyncWorker.getSyncWorker().syncAlamat(ctx,mApi.getAlamat(id_user), false);
                        //Toast.makeText(getActivity(), baseRespons.getMessage(), Toast.LENGTH_SHORT).show();
//                        Toast.makeText(ctx, "File Berhasil Di Hapus", Toast.LENGTH_SHORT).show();
                    }

                    Toast.makeText(ctx, baseRespons.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BaseRespons> call, Throwable t) {
                //Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void generate(List<Alamat> list) {
        clear();
        this.mList = list;
        notifyDataSetChanged();
    }

    public void clear() {
        mList.clear();
        notifyDataSetChanged();
    }
}
