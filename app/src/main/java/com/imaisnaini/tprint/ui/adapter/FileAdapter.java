package com.imaisnaini.tprint.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.imaisnaini.tprint.R;
import com.imaisnaini.tprint.bl.db.dao.FileDao;
import com.imaisnaini.tprint.bl.db.model.Files;
import com.imaisnaini.tprint.bl.network.api.Api;
import com.imaisnaini.tprint.bl.network.api.SyncWorker;
import com.imaisnaini.tprint.bl.network.config.RetrofitBuilder;
import com.imaisnaini.tprint.bl.network.model.BaseRespons;
import com.imaisnaini.tprint.ui.activity.MainActivity;
import com.imaisnaini.tprint.ui.util.PrefUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FileAdapter extends RecyclerView.Adapter<FileAdapter.ViewHolder>{
    private List<Files> mList = new ArrayList<>();
    Context ctx;
    int flag = 0;
    private Api mApi;

    public FileAdapter(Context ctx){
        this.ctx = ctx;
        mApi = RetrofitBuilder.builder(ctx).create(Api.class);
    }

    public FileAdapter(Context ctx, int flag) {
        this.ctx = ctx;
        this.flag = flag;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_file, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvTittle.setText(mList.get(position).getNama());
        holder.tvHalaman.setText(mList.get(position).getHalaman()+" Halaman");
        holder.tvSize.setText(mList.get(position).getSize()+" Kb");
        holder.id_file = mList.get(position).getId_file();
        holder.id_user = mList.get(position).getId_user();
        holder.position = position;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void removeItem(int position) {
        //int position = mList.indexOf(item);
        Files file = mList.get(position);
        try {
            FileDao.getFileDao().delete(file);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        mList.remove(position);
        notifyItemRemoved(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.item_file_tvFileName)
        TextView tvTittle;
        @BindView(R.id.item_file_tvHalaman)
        TextView tvHalaman;
        @BindView(R.id.item_file_tvSize)
        TextView tvSize;
        private String id_file;
        private String id_user;
        private int position;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.item_file_ivDelete) public void onClick(){
            MaterialDialog.Builder dialog = new MaterialDialog.Builder(ctx)
                    .title("Konfirmasi")
                    .content("Anda Yakin?")
                    .positiveText("Ya")
                    .negativeText("Tidak")
                    .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                        @Override
                        public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                            return false;
                        }
                    })
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                            super.onPositive(dialog);
                            if (flag == 0) {
                                deleteFile(id_user,tvTittle.getText().toString());
                                removeItem(position);
                                SyncWorker.getSyncWorker().syncFile(ctx,mApi.getFile(id_user), false);
                            }else {
                                dialog.dismiss();
                            }
                        }

                        @Override
                        public void onNegative(MaterialDialog dialog) {
                            super.onNegative(dialog);
                            dialog.dismiss();
                        }
                    });
            dialog.show();
        }
    }

    private void deleteFile(final String id_user, String file_name){
        mApi.deleteFile(id_user,file_name).enqueue(new Callback<BaseRespons>() {
            @Override
            public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                BaseRespons baseRespons = response.body();
                if(baseRespons != null) {
                    //Toast.makeText(getActivity(), user.getId_user(), Toast.LENGTH_SHORT).show();
                    if(!baseRespons.getError()) {
                        notifyDataSetChanged();
                        SyncWorker.getSyncWorker().syncFile(ctx,mApi.getFile(id_user), false);
                        //Toast.makeText(getActivity(), baseRespons.getMessage(), Toast.LENGTH_SHORT).show();
                        //Toast.makeText(getItemId(), "File Berhasil Di Hapus", Toast.LENGTH_SHORT).show();
                    }

//                    Toast.makeText(getActivity(), baseRespons.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BaseRespons> call, Throwable t) {
                //Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void generate(List<Files> list) {
        clear();
        this.mList = list;
        notifyDataSetChanged();
    }

    public void clear() {
        mList.clear();
        notifyDataSetChanged();
    }
}
