package com.imaisnaini.tprint.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.imaisnaini.tprint.R;
import com.imaisnaini.tprint.bl.db.model.Files;
import com.imaisnaini.tprint.bl.db.model.Produk;
import com.imaisnaini.tprint.bl.db.model.Transaksi;
import com.imaisnaini.tprint.bl.network.config.Config;
import com.imaisnaini.tprint.ui.activity.DetailTransaksiActivity;
import com.imaisnaini.tprint.ui.presenter.FilePresenter;
import com.imaisnaini.tprint.ui.presenter.ProdukPresenter;
import com.imaisnaini.tprint.ui.util.Currency;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.ViewHolder> {
    private List<Transaksi> mList = new ArrayList<>();
    Context context;

    public TransactionAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        Files mFile = new FilePresenter().getByID(mList.get(i).getId_file());
        Produk mProduk = new ProdukPresenter().getProdukByID(mList.get(i).getId_produk());
        holder.idTransaksi = mList.get(i).getId_transaksi();
        holder.status = mList.get(i).getStatus();
        holder.tvFileName.setText(mFile.getNama());
        holder.tvDate.setText(mList.get(i).getTgl_pesan());
        holder.tvPrice.setText(Currency.formatDefCurrency(mList.get(i).getHarga_total()));
        Picasso.get().load(Config.API_ICON_KATEGORI + mProduk.getIcon()).into(holder.ivIcon);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.ivIcon_itemHistory)
        ImageView ivIcon;
        @BindView(R.id.tvDate_itemHistory)
        TextView tvDate;
        @BindView(R.id.tvPrice_itemHistory)
        TextView tvPrice;
        @BindView(R.id.tvFileName_itemHistory)
        TextView tvFileName;
        private String idTransaksi;
        private String status;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.cardView_itemHistory) public void goDetail(){
            Intent intent = new Intent(context, DetailTransaksiActivity.class);
            intent.putExtra("id", idTransaksi);
            context.startActivity(intent);
        }
    }

    public void generate(List<Transaksi> list) {
        clear();
        this.mList = list;
        notifyDataSetChanged();
    }

    public void clear() {
        mList.clear();
        notifyDataSetChanged();
    }
}
