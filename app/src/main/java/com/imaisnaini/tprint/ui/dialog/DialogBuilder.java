package com.imaisnaini.tprint.ui.dialog;


import android.content.Context;
import android.support.annotation.NonNull;
import android.text.InputType;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.imaisnaini.tprint.R;

import java.util.List;

public class DialogBuilder {

    public static MaterialDialog showLoadingDialog(Context ctx, String title, String content, boolean isCircularProgress) {
        MaterialDialog.Builder dialog = new MaterialDialog.Builder(ctx)
                .title(title)
                .content(content)
                .progress(true, 0)
                .progressIndeterminateStyle(isCircularProgress);
        return dialog.show();
    }

    public static void showErrorDialog(Context ctx, String content) {
        new MaterialDialog.Builder(ctx)
                .title("Error")
                .content(content)
                .positiveText("OK")
                .show();
    }

    public static MaterialDialog showInputDialog(final Context context, int resTitle, int resHint, MaterialDialog.InputCallback callback){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(context)
                .title(resTitle)
                .inputType(InputType.TYPE_CLASS_TEXT)
                .input(resHint, 0, false, callback)
                .cancelable(false);
        return builder.show();
    }

    public static MaterialDialog alertDialog(final Context ctx, String content, MaterialDialog.SingleButtonCallback callback){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(ctx)
                .content(content)
                .negativeText("DENY")
                .positiveText("OK")
                .onPositive(callback);
        return builder.show();
    }

    public static MaterialDialog listDialog(final Context context, String title, MaterialDialog.ListCallbackSingleChoice listCallback, MaterialDialog.SingleButtonCallback callback){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(context)
                .title(title)
                .items(R.array.toogle_print)
                .itemsCallbackSingleChoice(
                        0,
                        listCallback)
                .positiveText(R.string.ok)
                .negativeText(R.string.cancel)
                .onPositive(callback)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                });
        return builder.show();
    }
}
