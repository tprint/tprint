package com.imaisnaini.tprint.ui.fragment;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.imaisnaini.tprint.R;
import com.imaisnaini.tprint.bl.db.dao.AlamatDao;
import com.imaisnaini.tprint.bl.db.model.Alamat;
import com.imaisnaini.tprint.bl.network.api.Api;
import com.imaisnaini.tprint.bl.network.api.SyncWorker;
import com.imaisnaini.tprint.bl.network.config.RetrofitBuilder;
import com.imaisnaini.tprint.bl.network.model.BaseRespons;
import com.imaisnaini.tprint.bl.network.model.User;
import com.imaisnaini.tprint.ui.activity.AlamatActivity;
import com.imaisnaini.tprint.ui.activity.MainActivity;
import com.imaisnaini.tprint.ui.dialog.DialogBuilder;
import com.imaisnaini.tprint.ui.presenter.AlamatPresenter;
import com.imaisnaini.tprint.ui.util.PrefUtil;

import java.sql.SQLException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AlamatFragment extends Fragment {
    @BindView(R.id.etNamaAlamat_fragmentAlamat)
    EditText etNamaAlamat;
    @BindView(R.id.etAlamat_fragmentAlamat)
    EditText etAlamat;
    @BindView(R.id.etProvinsi_fragmentAlamat)
    EditText etProvinsi;
    @BindView(R.id.etKota_fragmentAlamat)
    EditText etKota;
    @BindView(R.id.etKecamatan_fragmentAlamat)
    EditText etKecamatan;
    private Integer idAlamat;
    private String nama,alamat,provinsi,kota,kecamatan;
    private Api mApi;
    private User user;
    private boolean isFirstRun = true;

    public AlamatFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_alamat, container, false);
        ButterKnife.bind(this,view);
        mApi = RetrofitBuilder.builder(getActivity()).create(Api.class);
        user = PrefUtil.getUser(getContext(), PrefUtil.USER_SESSION);
        init();
        return view;
    }

    @OnClick(R.id.btnSave_fragmentAlamat)
    public void saveAct(){
        if(idAlamat==0){
            uploadData();
        } else {
            updateData();
        }

//        SyncWorker.getSyncWorker().syncAlamat(getActivity(),mApi.getAlamat(user.getId_user()), isFirstRun);
    }

    public void init(){
        AlamatActivity activity = (AlamatActivity) getActivity();
        idAlamat = activity.getIdAlamat();
        AlamatPresenter alamatPresenter = new AlamatPresenter();
        Alamat alamat = alamatPresenter.getByID(idAlamat);
        if(idAlamat != 0){
            etNamaAlamat.setText(alamat.getNama());
            etProvinsi.setText(alamat.getProvinsi());
            etKota.setText(alamat.getKota());
            etKecamatan.setText(alamat.getKecamatan());
            etAlamat.setText(alamat.getAlamat());
        }
    }

    public void toProfile(){
        ProfileFragment profileFragment = new ProfileFragment();
        final FragmentTransaction FT = getFragmentManager().beginTransaction();
        FT.replace(R.id.fragment_container, profileFragment);
        FT.commit();
    }

    private void uploadData(){
        nama = etNamaAlamat.getText().toString();
        alamat = etAlamat.getText().toString();
        provinsi = etProvinsi.getText().toString();
        kota = etKota.getText().toString();
        kecamatan = etKecamatan.getText().toString();

        mApi.addAlamat(user.getIdUser(),nama,alamat,provinsi,kota,kecamatan).enqueue(new Callback<BaseRespons>() {
            @Override
            public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                BaseRespons baseRespons = response.body();

                if (baseRespons != null) {
                    if (!baseRespons.getError()) {
//                        Toast.makeText(getActivity(), baseRespons.getMessage(), Toast.LENGTH_SHORT).show();
                        new DoCloudSync(getActivity()).execute();
                    }
                }
                if (response.code() == 403){
                    etNamaAlamat.requestFocus();
                    etNamaAlamat.setError(getString(R.string.error_login));
                }
            }

            @Override
            public void onFailure(Call<BaseRespons> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateData(){
        nama = etNamaAlamat.getText().toString();
        alamat = etAlamat.getText().toString();
        provinsi = etProvinsi.getText().toString();
        kota = etKota.getText().toString();
        kecamatan = etKecamatan.getText().toString();
        AlamatPresenter alamatPresenter = new AlamatPresenter();
        final Alamat alamatLama = alamatPresenter.getByID(idAlamat);

        mApi.updateAlamat(user.getIdUser(),idAlamat,nama,alamat,provinsi,kota,kecamatan).enqueue(new Callback<BaseRespons>() {
            @Override
            public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                BaseRespons baseRespons = response.body();

                if (baseRespons != null) {
                    if (!baseRespons.getError()) {
//                        Toast.makeText(getActivity(), baseRespons.getMessage(), Toast.LENGTH_SHORT).show();
                        try {
                            AlamatDao.getAlamatDao().delete(alamatLama);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        new DoCloudSync(getActivity()).execute();
                    }
                }
                if (response.code() == 403){
                    etNamaAlamat.requestFocus();
                    etNamaAlamat.setError(getString(R.string.error_login));
                }
            }

            @Override
            public void onFailure(Call<BaseRespons> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private class DoCloudSync extends AsyncTask<Void, Void, Void> {
        private MaterialDialog dialog;
        private final Context ctx;

        private DoCloudSync(Context ctx) {
            this.ctx = ctx;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = DialogBuilder.showLoadingDialog(ctx, "Updating Data", "Please Wait", false);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                SyncWorker.getSyncWorker().syncAlamat(ctx,mApi.getAlamat(user.getIdUser()), isFirstRun);
                if(isFirstRun) Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            toProfile();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dialog.dismiss();
        }
    }

}
