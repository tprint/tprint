package com.imaisnaini.tprint.ui.fragment;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.imaisnaini.tprint.R;
import com.imaisnaini.tprint.event.TransaksiTabEvent;
import com.imaisnaini.tprint.ui.adapter.TabAdapter;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment {
    private TabAdapter mTabAdapter;
    private TransactionFragment mDoingFragment, mDoneFragment, mDeliveryFragment;

    @BindView(R.id.viewPager_fragmentHistory) ViewPager viewPager;
    @BindView(R.id.tabLayout_fragmentHistory) TabLayout tabLayout;
    @BindString(R.string.doing_fragment) String labelTabDoing;
    @BindString(R.string.delivery_fragment) String labelTabDelivery;
    @BindString(R.string.done_fragment) String labelTabDone;

    public HistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        ButterKnife.bind(this, view);
        init();
        initListener();
        return view;
    }
    // This method to initialaze view
    private void init(){
        mTabAdapter = new TabAdapter(getFragmentManager());

        mTabAdapter.addFragment( new TransactionFragment(), labelTabDoing);
        mTabAdapter.addFragment( new TransactionFragment(), labelTabDelivery);
        mTabAdapter.addFragment( new TransactionFragment(), labelTabDone);

        viewPager.setAdapter(mTabAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void initListener(){
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                EventBus.getDefault().post(new TransaksiTabEvent(i));
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });
    }
}
