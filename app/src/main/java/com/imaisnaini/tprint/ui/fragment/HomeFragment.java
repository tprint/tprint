package com.imaisnaini.tprint.ui.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.imaisnaini.tprint.R;
import com.imaisnaini.tprint.bl.db.dao.FileDao;
import com.imaisnaini.tprint.bl.db.model.Files;
import com.imaisnaini.tprint.bl.db.model.Promo;
import com.imaisnaini.tprint.bl.network.config.Config;
import com.imaisnaini.tprint.ui.activity.ListMitraActivity;
import com.imaisnaini.tprint.ui.activity.MapActivity;
import com.imaisnaini.tprint.ui.presenter.PromoPresenter;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageClickListener;
import com.synnapps.carouselview.ImageListener;
import com.synnapps.carouselview.ViewListener;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    @BindView(R.id.tvTotalFile_fragmentHome)
    TextView totalFile;
    @BindView(R.id.carousel_fragmentHome)
    CarouselView carouselView;
    @BindView(R.id.etLokasi_fragmentHome)
    EditText etLokasi;
    List<Promo> promoList = new ArrayList<>();
    PromoPresenter promoPresenter;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        setTotalFile();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this,view);
        setTotalFile();
        initPresenter();
        initData();
        initListener();
        return view;
    }

    private int getFileCount(){
        List<Files> filesList = null;
        try {
            filesList = FileDao.getFileDao().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return filesList.size();
    }

    private void setTotalFile(){
        if(getFileCount()<1){
            totalFile.setText("Belum ada File yang diupload");
        } else {
            totalFile.setText(getFileCount()+" File telah diupload");
        }
    }

    private void initPresenter(){
        promoPresenter = new PromoPresenter();
    }

    private void initData(){
        promoList = promoPresenter.getPromoList();
    }

    private void initListener(){
        ImageListener imageListener = new ImageListener() {
            @Override
            public void setImageForPosition(int position, ImageView imageView) {
                Picasso.get().load(Config.API_ICON_PROMOSI + promoList.get(position).getBanner()).into(imageView);
                //imageView.setImageResource(img[position]);
            }
        };
        carouselView.setPageCount(promoList.size());
        carouselView.setImageListener(imageListener);
        carouselView.setImageClickListener(new ImageClickListener() {
            @Override
            public void onClick(int position) {
                Toast.makeText(getActivity(), promoList.get(0).getNama(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @OnClick(R.id.cvTotalFile__fragmentHome) public void toListMitra(){
        goMaps();
    }

    @OnClick(R.id.btnNext_fragmentHome) public void goMaps(){
        Intent intent=new Intent(getActivity(), ListMitraActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.etLokasi_fragmentHome) public void openMaps(){
        Intent intent=new Intent(getActivity(), MapActivity.class);
        startActivity(intent);
    }
}
