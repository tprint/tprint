package com.imaisnaini.tprint.ui.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.imaisnaini.tprint.R;
import com.imaisnaini.tprint.bl.db.model.Alamat;
import com.imaisnaini.tprint.bl.network.api.Api;
import com.imaisnaini.tprint.bl.network.model.User;
import com.imaisnaini.tprint.ui.activity.AlamatActivity;
import com.imaisnaini.tprint.ui.adapter.AlamatAdapter;
import com.imaisnaini.tprint.ui.presenter.AlamatPresenter;
import com.imaisnaini.tprint.ui.util.PrefUtil;
import com.imaisnaini.tprint.ui.view.AlamatView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment implements AlamatView {
    @BindView(R.id.btnEdit_fragmentProfile)
    FloatingActionButton btnEdit;
    @BindView(R.id.etNama_fragmentProfile)
    EditText etNama;
    @BindView(R.id.tvAddAlamat)
    TextView tvAddAlamat;
    @BindView(R.id.etEmail_fragmentProfile)
    EditText etEmail;
    @BindView(R.id.etPhone_fragmentProfile)
    EditText etPhone;
    @BindView(R.id.rvContent_fragmentProfile)
    RecyclerView mRvAlamat;

    private AlamatAdapter mAdapter;
    private AlamatPresenter alamatPresenter;
    private List<Alamat> mList = new ArrayList<>();
    private Api mApi;
    private User user;
    private Boolean mode = true;

    int flag = 0;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        init();
        initData();
        return view;
    }

    private void initData(){
        user = PrefUtil.getUser(getContext(), PrefUtil.USER_SESSION);
        if (user != null){
            etEmail.setText(user.getEmail());
            etNama.setText(user.getNama());
            etPhone.setText(user.getTelp());
        }
    }

    @OnClick(R.id.btnEdit_fragmentProfile) public void onEdit(){
        if (mode) {
            etEmail.setEnabled(true);
            etNama.setEnabled(true);
            etPhone.setEnabled(true);
            etEmail.requestFocus();
            btnEdit.setImageResource(R.drawable.ic_check);
            mode = false;
        }else{
            etEmail.setEnabled(false);
            etNama.setEnabled(false);
            etPhone.setEnabled(false);
            btnEdit.setImageResource(R.drawable.ic_edit);
            mode = true;
        }
    }

    public void init(){
        mRvAlamat.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRvAlamat.setHasFixedSize(true);
        mAdapter = new AlamatAdapter(getContext());

        alamatPresenter = new AlamatPresenter(this);
    }

    @OnClick(R.id.tvAddAlamat)
    public void toAddAlamat(){
        Intent i = new Intent(getContext(), AlamatActivity.class);
        i.putExtra("id_alamat", 0);
        startActivity(i);
//        startActivity(new Intent(getContext(), AlamatActivity.class));
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadAlamat(List<Alamat> alamatList) {
        mAdapter.generate(alamatList);

        mRvAlamat.setAdapter(mAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        alamatPresenter.loadData();
    }
}
