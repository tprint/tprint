package com.imaisnaini.tprint.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.imaisnaini.tprint.R;
import com.imaisnaini.tprint.bl.network.api.Api;
import com.imaisnaini.tprint.bl.network.config.RetrofitBuilder;
import com.imaisnaini.tprint.bl.network.model.BaseRespons;
import com.imaisnaini.tprint.bl.network.model.User;
import com.imaisnaini.tprint.ui.activity.MainActivity;
import com.imaisnaini.tprint.ui.util.PrefUtil;
import com.imaisnaini.tprint.ui.util.validation.EmailValidator;
import com.imaisnaini.tprint.ui.util.validation.PasswordValidator;
import com.imaisnaini.tprint.ui.util.validation.PhoneValidator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.text.TextUtils.isEmpty;


public class RegisterFragment extends Fragment {
    @BindView(R.id.etEmail_fragmentRegister) TextInputEditText etEmail;
    @BindView(R.id.etPassword_fragmentRegister) TextInputEditText etPass;
    @BindView(R.id.etNama_fragmentRegister) TextInputEditText etNama;
    @BindView(R.id.etPhone_fragmentRegister)TextInputEditText etPhone;

    private String password,nama,email,telp;
    private Api mApi;


    PhoneValidator phoneValidator;
    PasswordValidator passwordValidator;
    EmailValidator emailValidator;
    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this,view);
        mApi = RetrofitBuilder.builder(getActivity()).create(Api.class);
        return view;
    }

    @OnClick(R.id.btnRegister_fragmentRegister)
    public void register(){
        if (checkDataEntered()){
            registerAct();
        }
    }

    private void registerAct(){
        nama = etNama.getText().toString();
        email = etEmail.getText().toString();
        telp = etPhone.getText().toString();
        password = etPass.getText().toString();

        mApi.register(nama, email, telp, password).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User user = response.body();

                if(user != null) {
                    createStorage(user.getIdUser());
                    //Toast.makeText(getActivity(), user.getIdUser(), Toast.LENGTH_SHORT).show();
                    if(!user.getError()) {
                        PrefUtil.putUser(getActivity(), PrefUtil.USER_SESSION,user);
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }

                    Toast.makeText(getActivity(), user.getMessage(), Toast.LENGTH_SHORT).show();
                }
                if (response.code() == 403){
                    etEmail.requestFocus();
                    etEmail.setError(getString(R.string.error_login));
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void createStorage(String id_user){
        mApi.createStorage(id_user).enqueue(new Callback<BaseRespons>() {
            @Override
            public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                BaseRespons baseRespons = response.body();

                if(baseRespons != null) {
                    //Toast.makeText(getActivity(), user.getIdUser(), Toast.LENGTH_SHORT).show();
                    if(!baseRespons.getError()) {
                        Toast.makeText(getActivity(), baseRespons.getMessage(), Toast.LENGTH_SHORT).show();
                    }

//                    Toast.makeText(getActivity(), baseRespons.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BaseRespons> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    boolean isEempty(EditText text){
        CharSequence str = text.getText().toString();
        return isEmpty(str);
    }

    boolean isPhoneNum(EditText text){
        phoneValidator = new PhoneValidator();
        String number = text.getText().toString();
        return phoneValidator.isValid(number);
    }

    boolean isEmail(EditText text){
        emailValidator = new EmailValidator();
        String email = text.getText().toString();
        return emailValidator.isValid(email);
    }

    boolean isPassword(EditText text){
        passwordValidator = new PasswordValidator();
        String pass = text.getText().toString();
        return passwordValidator.isValid(pass);
    }

    private boolean checkDataEntered(){
        if(isEempty(etNama)){
            etNama.setError("Nama harus diisi");
            return false;
        }
        if(isEempty(etPhone)){
            etPhone.setError("Nomor Telepon harus diisi");
            return false;
        }
        if(!isPhoneNum(etPhone)){
            String str = phoneValidator.getString();
            etPhone.setError(str);
            return false;
        }
        if(isEempty(etEmail)){
            etEmail.setError("Email harus diisi");
            return false;
        }
        if(!isEmail(etEmail)){
            etEmail.setError("Email tidak valid");
            return false;
        }
        if(isEempty(etPass)){
            etPass.setError("Password harus diisi");
            return false;
        }
        if(!isPassword(etPass)){
            String str = passwordValidator.getString();
            Toast.makeText(getActivity(),str, Toast.LENGTH_SHORT).show();
        }
        return true;
    }
}
