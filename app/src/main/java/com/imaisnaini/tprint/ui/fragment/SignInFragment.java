package com.imaisnaini.tprint.ui.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.imaisnaini.tprint.R;
import com.imaisnaini.tprint.bl.network.api.Api;
import com.imaisnaini.tprint.bl.network.config.RetrofitBuilder;
import com.imaisnaini.tprint.bl.network.model.StorageGet;
import com.imaisnaini.tprint.bl.network.model.User;
import com.imaisnaini.tprint.ui.activity.MainActivity;
import com.imaisnaini.tprint.ui.dialog.DialogBuilder;
import com.imaisnaini.tprint.ui.util.PrefUtil;
import com.imaisnaini.tprint.ui.util.validation.EmailValidator;
import com.imaisnaini.tprint.ui.util.validation.PasswordValidator;
import com.imaisnaini.tprint.ui.util.validation.PhoneValidator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.text.TextUtils.isEmpty;


/**
 * A simple {@link Fragment} subclass.
 */
public class SignInFragment extends Fragment {

    @BindView(R.id.etUsername_fragmentSignIn) EditText etUsername;
    @BindView(R.id.etPassword_fragmentSignIn) EditText etPassword;

    EmailValidator emailValidator;
    PhoneValidator phoneValidator;
    PasswordValidator passwordValidator;

    private String username;
    private String password;
    private Api mApi;

    public SignInFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // if user logged in move to main activity
        if (isSessionLogin()){
            startActivity(new Intent(getActivity(), MainActivity.class));
            getActivity().finish();
        }

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sign_in, container, false);
        ButterKnife.bind(this, view);
        mApi = RetrofitBuilder.builder(getActivity()).create(Api.class);

        return view;
    }

    boolean isEempty(EditText text){
        CharSequence str = text.getText().toString();
        return isEmpty(str);
    }

    boolean isUsername(EditText text){
        emailValidator = new EmailValidator();
        phoneValidator = new PhoneValidator();
        String username = text.getText().toString();
        return emailValidator.isValid(username) || phoneValidator.isValid(username);
    }

    boolean isPassword(EditText text){
        passwordValidator = new PasswordValidator();
        String pass = text.getText().toString();
        return passwordValidator.isValid(pass);
    }

    @OnClick(R.id.btnSignIn_fragmentSignIn)
    void onLogin(){
        if(isEempty(etUsername)){
            etUsername.setError("Email harus diisi");
        }
        if(isEempty(etPassword)){
            etPassword.setError("Password harus diisi");
        }
        if(!isUsername(etUsername)){
            etUsername.setError("Email tidak valid");
        }
        if(!isPassword(etPassword)){
            String str = passwordValidator.getString();
            Toast.makeText(getActivity(),str, Toast.LENGTH_SHORT).show();
        }
        DialogBuilder.showLoadingDialog(getContext(), "Updating Data", "Please wait..", false);
        loginAct();
    }


    void loginAct(){
        username = etUsername.getText().toString();
        password = etPassword.getText().toString();
        mApi.login(username, password).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User user = response.body();
                Log.i("USER_LOGIN", response.message());

                if (user != null){
                    if (!user.getError()){
                        PrefUtil.putUser(getActivity(), PrefUtil.USER_SESSION, user);
                        readStorage(user.getIdUser());
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }
                    Toast.makeText(getActivity(), user.getMessage(), Toast.LENGTH_SHORT).show();
                }
                if (response.code() == 403){
                    etPassword.requestFocus();
                    etPassword.setError(getString(R.string.error_password));
                }
                if (response.code() == 404){
                    etUsername.requestFocus();
                    etUsername.setError(getString(R.string.error_login));
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.i("USER_LOGIN", t.getMessage());
            }
        });
    }

    private void readStorage(String id_user){
        mApi.readStorage(id_user).enqueue(new Callback<StorageGet>() {
            @Override
            public void onResponse(Call<StorageGet> call, Response<StorageGet> response) {
                StorageGet storageRespons = response.body();

                if(storageRespons != null) {
                    //Toast.makeText(getActivity(), user.getId_user(), Toast.LENGTH_SHORT).show();
                    if(!storageRespons.getError()) {
                        //Toast.makeText(getActivity(), baseRespons.getMessage(), Toast.LENGTH_SHORT).show();
                        //Toast.makeText(getActivity(), "Success Read Storage", Toast.LENGTH_SHORT).show();
                        PrefUtil.putStorage(getActivity(), PrefUtil.USER_STORAGE, storageRespons);
                    }
                }
            }

            @Override
            public void onFailure(Call<StorageGet> call, Throwable t) {
                //Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }



    // this method to check is user logged in ?
    boolean isSessionLogin(){
        return PrefUtil.getUser(getActivity(), PrefUtil.USER_SESSION) != null;
    }
}
