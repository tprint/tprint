package com.imaisnaini.tprint.ui.fragment;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.imaisnaini.tprint.R;
import com.imaisnaini.tprint.bl.db.dao.FileDao;
import com.imaisnaini.tprint.bl.db.model.Files;
import com.imaisnaini.tprint.bl.network.api.Api;
import com.imaisnaini.tprint.bl.network.api.SyncWorker;
import com.imaisnaini.tprint.bl.network.config.RetrofitBuilder;
import com.imaisnaini.tprint.bl.network.model.BaseRespons;
import com.imaisnaini.tprint.bl.network.model.StorageGet;
import com.imaisnaini.tprint.bl.network.model.User;
import com.imaisnaini.tprint.ui.adapter.FileAdapter;
import com.imaisnaini.tprint.ui.dialog.DialogBuilder;
import com.imaisnaini.tprint.ui.presenter.FilePresenter;
import com.imaisnaini.tprint.ui.util.PrefUtil;
import com.imaisnaini.tprint.ui.util.RealPathUtil;
import com.imaisnaini.tprint.ui.view.FileView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.io.File;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.support.constraint.Constraints.TAG;

/**
 * A fragment with a Google +1 button.
 */
public class StorageFragment extends Fragment implements FileView {

    private FileAdapter mAdapter;
    private FilePresenter filePresenter;
    private List<Files> mList = new ArrayList<>();
    private boolean isFirstRun = false;
    private static final int READ_REQUEST_CODE = 42;
    public static final int PICK_FILE = 100;
    private Api mApi;
    private User user;
    private StorageGet storage;
    private Uri selectedFile;
    private MaterialDialog dialogBuilder;

    int flag = 0;

    @BindView(R.id.rvFile_fragmentStorage)
    RecyclerView mRvFile;
    @BindView(R.id.fabtnUpload_fragmentStorage)
    FloatingActionButton mUpload;
    @BindView(R.id.progressBar_fragmentStorage)
    ProgressBar storageUsage;
    @BindView(R.id.tvPB_fragmentStorage)
    TextView tvPB;


    public StorageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_storage, container, false);
        ButterKnife.bind(this,view);
        mApi = RetrofitBuilder.builder(getActivity()).create(Api.class);
        user = PrefUtil.getUser(getContext(), PrefUtil.USER_SESSION);
        storage = PrefUtil.getStorage(getContext(), PrefUtil.USER_STORAGE);
        init();
        return view;
    }

    public void init(){
        mRvFile.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRvFile.setHasFixedSize(true);
        mAdapter = new FileAdapter(getActivity());
        filePresenter = new FilePresenter(this);
        setProgres();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadFile(List<Files> fileList){
        mAdapter.generate(fileList);

        mRvFile.setAdapter(mAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        clearFile();
        new DoCloudSync(getActivity()).execute();
        SyncWorker.getSyncWorker().syncFile(getContext(),mApi.getFile(user.getIdUser()), isFirstRun);
        filePresenter.loadData();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    public void setProgres() {
        Integer used = Integer.valueOf(storage.getSize());
        Integer space = Integer.valueOf(storage.getMaxSize());
        storageUsage.setProgress((used*100)/space);
        String strUsed = Integer.toString(used/1000000);
        String strSpace = Integer.toString(space/1000000);
        tvPB.setText(strUsed+"MB/"+strSpace+"MB");
    }

    @OnClick(R.id.fabtnUpload_fragmentStorage)
    public void performFileSearch() {

        String[] mimeTypes =
                {"application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                        "application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                        "application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                        "text/plain",
                        "application/pdf",
                        "image/*"};

        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        for (String type: mimeTypes) {
            i.setType(type);
        }
        if (mimeTypes.length > 0) {
            i.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        }
        startActivityForResult(i, PICK_FILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        if (requestCode == PICK_FILE && resultCode == RESULT_OK) {
            if (resultData != null) {
                selectedFile = resultData.getData();
                Log.i(TAG, "Uri: " + selectedFile.toString());
                //Toast.makeText(getActivity(), uri.toString(), Toast.LENGTH_SHORT).show();
                uploadFile(selectedFile);
            }
        }
    }

    private void uploadFile(Uri fileUri){
        File file = new File(RealPathUtil.getRealPath(getActivity(), fileUri));
        RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());
        RequestBody requestFile = RequestBody.create(MediaType.parse(getActivity().getContentResolver().getType(fileUri)), file);
        MultipartBody.Part reqFile = MultipartBody.Part.createFormData("file", file.getName(), requestFile);

//        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), file);
//        MultipartBody.Part reqFile = MultipartBody.Part.createFormData("file", file.getName(), requestBody);
//        RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());

        dialogBuilder = DialogBuilder.showLoadingDialog(getActivity(), "Uploading File", "Please Wait", false);
        mApi.uploadFile(user.getIdUser(), filename, reqFile).enqueue(new Callback<BaseRespons>() {
            @Override
            public void onResponse(Call<BaseRespons> call, Response<BaseRespons> response) {
                BaseRespons respons = response.body();
                Log.i("UPLOAD_FILE", response.message());

                if (respons != null) {
                    if (!respons.getError()) {
                        //Toast.makeText(getActivity(), respons.getMessage(), Toast.LENGTH_SHORT).show();
                        dialogBuilder.dismiss();
                        new DoCloudSync(getActivity()).execute();
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseRespons> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e("UPLOAD_FILE", t.getMessage());
            }
        });

        //hideLoading();
    }

    private void clearFile(){
        for (int i=0 ; i<mList.size();i++){
            try {
                FileDao.getFileDao().delete(mList.get(i));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private class DoCloudSync extends AsyncTask<Void, Void, Void> {
        private MaterialDialog dialog;
        private final Context ctx;

        private DoCloudSync(Context ctx) {
            this.ctx = ctx;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = DialogBuilder.showLoadingDialog(ctx, "Updating Data", "Please Wait", false);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                SyncWorker.getSyncWorker().syncFile(ctx,mApi.getFile(user.getIdUser()), isFirstRun);
                if(isFirstRun) Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            dialog.dismiss();
        }
    }
}
