package com.imaisnaini.tprint.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.imaisnaini.tprint.R;
import com.imaisnaini.tprint.bl.db.model.Transaksi;
import com.imaisnaini.tprint.event.TransaksiTabEvent;
import com.imaisnaini.tprint.ui.adapter.TransactionAdapter;
import com.imaisnaini.tprint.ui.presenter.TransaksiPresenter;
import com.imaisnaini.tprint.ui.view.TransaksiView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class TransactionFragment extends Fragment implements TransaksiView {
    private TransactionAdapter mAdapter;
    private TransaksiPresenter transaksiPresenter;
    private List<Transaksi> mList = new ArrayList<>();
    private String title;
    private int page;

    @BindView(R.id.rvContent_fragmentTransaction)
    RecyclerView mRvContent;
    @BindView(R.id.tvError_fragmentTransaction)
    TextView mTvError;

    @Subscribe(threadMode = ThreadMode.MAIN) public void setMode(TransaksiTabEvent event){
        page = event.idTab;
        loadData(page);
        Log.i("TRANSAKSI_MODE", "setMode: " + page);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public TransactionFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_transaction, container, false);
        ButterKnife.bind(this, view);
        init();
        EventBus.getDefault().register(this);
        return view;
    }

    private void init(){
        transaksiPresenter = new TransaksiPresenter(this);
        mAdapter = new TransactionAdapter(getContext());
        mRvContent.setHasFixedSize(true);
        mRvContent.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvContent.setAdapter(mAdapter);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void loadTransaksi(List<Transaksi> transaksiList) {

    }

    @Override
    public void onResume() {
        super.onResume();
        loadData(page);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    private void loadData(int i){
        mList.clear();
        if (i == 0) {
            mList = transaksiPresenter.getOrder();
        }else if (i == 1) {
            mList = transaksiPresenter.getDelivery();
        }else if (i == 2) {
            mList = transaksiPresenter.getHistory();
        }
        if(mList.size() > 0) {
            mAdapter.generate(mList);
            mAdapter.notifyDataSetChanged();
        }
        Log.i("TRANSAKSI_LIST", "loadData: " + mList.size());

        showError();
    }

    private void showError(){
        if (mList.size() == 0){
            mTvError.setText(getString(R.string.error_transaksi));
            mTvError.setVisibility(View.VISIBLE);
        }else {
            mTvError.setVisibility(View.GONE);
        }
    }
}
