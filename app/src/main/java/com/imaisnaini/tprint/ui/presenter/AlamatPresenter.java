package com.imaisnaini.tprint.ui.presenter;

import com.imaisnaini.tprint.bl.db.dao.AlamatDao;
import com.imaisnaini.tprint.bl.db.model.Alamat;
import com.imaisnaini.tprint.ui.view.AlamatView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AlamatPresenter {
    private AlamatView alamatView;

    public AlamatPresenter() {
    }

    public AlamatPresenter(AlamatView alamatView){
        this.alamatView=alamatView;
    }

    public void loadData(){
        alamatView.showLoading();

        List<Alamat> alamatList = new ArrayList<>();
        try {
            alamatList = AlamatDao.getAlamatDao().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        alamatView.loadAlamat(alamatList);
        alamatView.hideLoading();
    }

    public Alamat getByID(Integer id){
        Alamat alamat = null;
        try {
            alamat = AlamatDao.getAlamatDao().readByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return alamat;
    }

    public List<Alamat> getAlamatList(){
        List<Alamat> list = new ArrayList<>();
        try {
            list = AlamatDao.getAlamatDao().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
}
