package com.imaisnaini.tprint.ui.presenter;

import com.imaisnaini.tprint.bl.db.dao.FileDao;
import com.imaisnaini.tprint.bl.db.model.Files;
import com.imaisnaini.tprint.ui.view.FileView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FilePresenter {
    private FileView fileView;

    public FilePresenter() {
    }

    public FilePresenter(FileView fileView){
        this.fileView=fileView;
    }

    public void loadData(){
        fileView.showLoading();

        List<Files> filesList = new ArrayList<>();
        try {
            filesList = FileDao.getFileDao().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        fileView.loadFile(filesList);
        fileView.hideLoading();
    }

    public Files getByID(String id){
        Files files = null;
        try {
            files = FileDao.getFileDao().readByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return files;
    }

    public List<Files> getFileList(){
        List<Files> list = new ArrayList<>();
        try {
            list = FileDao.getFileDao().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
}
