package com.imaisnaini.tprint.ui.presenter;

import com.imaisnaini.tprint.ui.view.ProdukView;
import com.imaisnaini.tprint.bl.db.model.Produk;
import com.imaisnaini.tprint.bl.db.dao.ProdukDao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProdukPresenter {
    private ProdukView produkView;

    public ProdukPresenter() {
    }

    public ProdukPresenter(ProdukView produkView) {
        this.produkView = produkView;
    }

    public void loadProdukByMitra(String id_mitra){
        produkView.showLoading();
        List<Produk> produkList = new ArrayList<>();
        try {
            produkList = ProdukDao.getProdukDao().readByMitra(id_mitra);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        produkView.loadProduk(produkList);
        produkView.hideLoading();
    }

    public Produk getProdukByID(Integer id_produk){
        Produk produk = new Produk();
        try {
            produk = ProdukDao.getProdukDao().readByID(id_produk);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return produk;
    }
}
