package com.imaisnaini.tprint.ui.presenter;

import com.imaisnaini.tprint.bl.db.dao.PromoDao;
import com.imaisnaini.tprint.bl.db.model.Promo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PromoPresenter {
    public PromoPresenter() {
    }

    public List<Promo> getPromoList(){
        List<Promo> list = new ArrayList<>();
        try {
            list = PromoDao.getPromoDao().read();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public Promo getByID(String id){
        Promo promo = null;
        try {
            promo = PromoDao.getPromoDao().readByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return promo;
    }
}
