package com.imaisnaini.tprint.ui.presenter;

import android.util.Log;

import com.imaisnaini.tprint.bl.db.dao.TransaksiDao;
import com.imaisnaini.tprint.bl.db.model.Transaksi;
import com.imaisnaini.tprint.ui.view.TransaksiView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TransaksiPresenter {
    private TransaksiView transaksiView;

    public TransaksiPresenter() {
    }

    public TransaksiPresenter(TransaksiView transaksiView) {
        this.transaksiView = transaksiView;
    }

    public Transaksi getByID(String id){
        Transaksi transaksi = new Transaksi();
        try {
            transaksi = TransaksiDao.getTransaksiDao().getTransaksiByID(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return transaksi;
    }

    public List<Transaksi> getOrder(){
        transaksiView.showLoading();
        List<Transaksi> list = new ArrayList<>();
        try {
            list = TransaksiDao.getTransaksiDao().getByStatus("0");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Log.i("TRANSAKSI_LIST", "getDelivery: " + list.size());
        //transaksiView.loadTransaksi(list);
        transaksiView.hideLoading();
        return list;
    }

    public List<Transaksi> getDelivery(){
        transaksiView.showLoading();
        List<Transaksi> list = new ArrayList<>();
        try {
            list = TransaksiDao.getTransaksiDao().getByStatus("1");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Log.i("TRANSAKSI_LIST", "getDelivery: " + list.size());
        //transaksiView.loadTransaksi(list);
        transaksiView.hideLoading();
        return list;
    }

    public List<Transaksi> getHistory(){
        transaksiView.showLoading();
        List<Transaksi> list = new ArrayList<>();
        try {
            list = TransaksiDao.getTransaksiDao().getByStatus("3");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Log.i("TRANSAKSI_LIST", "getDelivery: " + list.size());
        //transaksiView.loadTransaksi(list);
        transaksiView.hideLoading();
        return list;
    }

    public void updateStatus(String id){
        transaksiView.showLoading();
    }
}
