package com.imaisnaini.tprint.ui.util;


import android.text.TextUtils;
import android.widget.TextView;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

/**
 * Created by Zeez on 13/09/2016.
 */
public class Currency {
    public static String formatDefCurrency(BigDecimal number) {
        return formatCurrency(number, null);
    }

    public static String formatCurrency(BigDecimal number, String format){
        if(TextUtils.isEmpty(format)){
            format = "###,###";
        }

        DecimalFormatSymbols mSymbol = new DecimalFormatSymbols();
        mSymbol.setDecimalSeparator('.');

        DecimalFormat mDecimalFormat = new DecimalFormat(format, mSymbol);
        String s = mDecimalFormat.format(number);
        return s;
    }

    public static void TextCurrencyProvider(CharSequence charseq, TextView thisText, String pattern, Locale locale) {
        StringBuilder sbValue = new StringBuilder();
        NumberFormat format = NumberFormat.getCurrencyInstance(locale);
        DecimalFormatSymbols custSymbols = new DecimalFormatSymbols(locale);
        DecimalFormat decFormatter = (DecimalFormat) format;
        custSymbols.setDecimalSeparator('.');
        custSymbols.setGroupingSeparator(',');
        decFormatter.applyPattern(pattern);
        decFormatter.setDecimalFormatSymbols(custSymbols);
        Number val;
        try {
            val = format.parse(charseq.toString().replace(",", ""));
            BigDecimal toNumber = new BigDecimal(val.doubleValue());
            sbValue.append(decFormatter.format(toNumber));
            thisText.setText(sbValue.toString());
        } catch (ParseException e) {
            System.out.println("Input Pembayaran Tidak Boleh Kosong");
        }
    }
}
