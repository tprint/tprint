package com.imaisnaini.tprint.ui.util.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneValidator {
    final String PASSWORD_PATTERN = "[0-9]{8,13}";
    Pattern pattern;
    Matcher matcher;

    private String str;
    public boolean isValid(String number) {
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(number);
        if (!matcher.matches()) {
            setString("PNomor Telepon Tidak Valid");
            return false;
        }
        return true;
    }

    public void setString(String str){
        this.str = str;
    }

    public String  getString(){
        return str;
    }
}
