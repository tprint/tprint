package com.imaisnaini.tprint.ui.view;

import com.imaisnaini.tprint.bl.db.model.Alamat;

import java.util.List;

public interface AlamatView {
    void showLoading();
    void hideLoading();
    void loadAlamat(List<Alamat> alamatList);
}
