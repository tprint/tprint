package com.imaisnaini.tprint.ui.view;

import com.imaisnaini.tprint.bl.db.model.Files;

import java.util.List;

public interface FileView {
    void showLoading();
    void hideLoading();
    void loadFile(List<Files> filesList);
}
