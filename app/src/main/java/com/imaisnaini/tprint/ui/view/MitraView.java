package com.imaisnaini.tprint.ui.view;

import com.imaisnaini.tprint.bl.db.model.Mitra;
import java.util.*;

public interface MitraView {
    void showLoading();
    void hideLoading();
    void loadMitra(List<Mitra> mitraList);
}
