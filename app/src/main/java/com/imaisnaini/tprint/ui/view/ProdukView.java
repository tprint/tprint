package com.imaisnaini.tprint.ui.view;

import com.imaisnaini.tprint.bl.db.model.Produk;
import java.util.List;

public interface ProdukView {
    void showLoading();
    void hideLoading();
    void loadProduk(List<Produk> produkList);
}
