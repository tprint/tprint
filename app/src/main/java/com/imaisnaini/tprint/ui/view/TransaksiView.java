package com.imaisnaini.tprint.ui.view;

import com.imaisnaini.tprint.bl.db.model.Transaksi;

import java.util.List;

public interface TransaksiView {
    void showLoading();
    void hideLoading();
    void loadTransaksi(List<Transaksi> transaksiList);
}
